let Api = require("./http/api.js");
let request = require("./http/request.js");
let config = require("./env/index.js")
let router = require("./utils/router.js")
let env = "Test";
App.version = "1.0.0";
App.config = config[env]; // 公共文件用的


//app.js
App({
  config:config[env], // 给视图用的
  Api,
  router,
  get:request.fetch,
  post:(url,data,option) =>{
    option.method = "post";
    return request.fetch(url,data,option);
  },

  //onLaunch,onShow: options(path,query,scene,shareTicket,referrerInfo(appId,extraData))
  onLaunch: function(options) {
   // console.log("apponlunch")
  //  wx.setStorageSync("ip", 123)

   // console.log(wx.getStorageSync('ip'))
  },
  onShow: function(options) {
  //console.log("jw")
  let env = App.config.baseApi;
  const token=wx.getStorageSync("token");
    wx.request({
      url: env+"/jwt/jwtveriitfy",
      data:{token:token},
      success(res){
      //console.log(res.data.data)
      let status=res.data.data;
      if(status=="login"){
        //console.log("loginok")
      }else {
       // console.log("logout")
        wx.showToast({
          title: '登录过期请重新登录',
        })
        wx.clearStorageSync()
        wx.reLaunch({
          url: '/pages/user/user'
        })
      }
      },fail(){
        console.log("fail")
      }
    })


  },
  onHide: function() {
    
  },
  onError: function(msg) {
  
  },
  //options(path,query,isEntryPage)
  onPageNotFound: function(options) {
   
  }
 
});
  