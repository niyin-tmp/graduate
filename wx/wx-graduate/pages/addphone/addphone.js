// pages/addphone/addphone.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone:"",
    newphone:"",
    code:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const token=wx.getStorageSync("token");
    app.get(Api.getPhone,{token:token}).then(res=>{
      console.log(res)
      if(res.phone==""){
        console.log("无号码")
      }else{
        console.log(res.phone)
        this.setData({
          phone:res.phone
        })
      }
    })
  },
  formSubmit(e){
    console.log(e.detail.value.inputphone)
    if(e.detail.value.inputphone!=""){
      let phone=e.detail.value.inputphone
      var strRegex = /^(13|14|15|17|18)\d{9}$/;
      if(!strRegex.test(phone)){
        console.log("请输入正确号码")
        this.setData({
          newphone:""
        })
        return
      }else{
        const token=wx.getStorageSync("token");
        app.get(Api.getcode,{token:token,phone:phone,type:"0"}).then(res=>{
          console.log(res)
          let newphone=phone
          this.setData({
            newphone:newphone
          })
        })
      }
    }else{
      console.log("请输入号码")
      this.setData({
        newphone:""
      })
      return
      }
  },
  verifyinput(e){
    console.log(e.detail.value)
    let code=e.detail.value
    this.setData({
      code:code
    })
  },
  submit(){
    console.log(this.data.newphone)
    if(this.data.newphone!=""&&this.data.code!=""){
      const token=wx.getStorageSync("token");
      let code=this.data.code
      let phone=this.data.newphone
      app.get(Api.verifycode,{token:token,phone:phone,code:code,type:"0"}).then(res=>{
        console.log(res)
        if(res=="ok"){
          console.log("ok")
          wx.navigateBack();
        }
      })
    }else if(this.data.code==""){
      console.log("请输入验证码")
      return
    }
    else{
      console.log("请重新获取验证码")
    }
  },
  relieve(){
    const token=wx.getStorageSync("token");
    app.get(Api.relievephone,{token:token}).then(res=>{
      console.log(res)
      if(res){
        this.setData({
          phone:""
        })
        this.onShow();
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})