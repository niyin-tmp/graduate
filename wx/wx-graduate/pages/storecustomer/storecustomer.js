// pages/storecustomer/storecustomer.js
const app = getApp()
let Api = app.Api
var interval=null;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    useropenid:"",
    id:"",
    username:"",
    toid:"toid",
    list:[],
    inputinfo:"",
    rawData_avatarUrl:""
  },
  formSubmit(e){
    let that=this
    const token=wx.getStorageSync("token");
    console.log(e.detail.value.in)
    let content=e.detail.value.in
    console.log(this.data.useropenid)
    let useropenid=this.data.useropenid
    let contype="1"
    let env = App.config.baseApi;
    app.get(Api.setstorechathistory,{token:token,content:content,useropenid:useropenid,contype:contype}).then(res=>{
      console.log(res)
      wx.request({
        url:env+"/ChatHistory/getstorechathistory",
        data:{"token":token,"useropenid":useropenid},
        header:{
           // "Content-Type":"application/json"
        },
        success:function(res){
            console.log(res.data.data.length)
            let id="";
            for(let i=0;i<res.data.data.length;i++){
              id=res.data.data[i].id
             // console.log(res.data.data[i].id)
             }
             that.setData({
              id:"a"+id,
              list:res.data.data
             })
            that.setData({
              list:res.data.data
             })
        },
        fail:function(err){
            console.log(err)
        }
  
    })
   
    })
    this.setData({
      inputinfo:""
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   let that=this
  //console.log(options)
  let username=options.username
  let useropenid=options.useropenid
  const token=wx.getStorageSync("token");
  const rawData_avatarUrl=wx.getStorageSync("rawData_avatarUrl");
  app.get(Api.getstorechathistory,{token:token,useropenid:useropenid}).then(res=>{
   // console.log(res)
    let id="";
    for(let i=0;i<res.length;i++){
     id=res[i].id
    }
    that.setData({
      username:username,
      useropenid:useropenid,
      rawData_avatarUrl:rawData_avatarUrl,
      id:"a"+id,
      list:res
    })
  })
  this.setCountdown(useropenid)
  },
 
// 设置每秒一次。
setCountdown: function (e) {
  //因为interval函数中的this域不一样了，需要把页面的this提取出来
  console.log("yyyyyyyyy")
  console.log(e)
  var that = this;  
  const token=wx.getStorageSync("token");
  let useropenid=e
  let env = App.config.baseApi;
  
  interval = setInterval(function () {
   // console.log(useropenid)
    //常规请求
    wx.request({
      url:env+"/ChatHistory/getstorechathistory",
      data:{"token":token,"useropenid":useropenid},
      header:{
         // "Content-Type":"application/json"
      },
      success:function(res){
         // console.log(res.data.data.length)
          let id="";
          for(let i=0;i<res.data.data.length;i++){
            id=res.data.data[i].id
            //console.log(res.data.data[i].id)
           }
           that.setData({
            id:"a"+id,
            list:res.data.data
           })
          that.setData({
            list:res.data.data
           })
      },
      fail:function(err){
          console.log(err)
      }

  })
  }, 5000)
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    clearInterval(interval);
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(interval);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})