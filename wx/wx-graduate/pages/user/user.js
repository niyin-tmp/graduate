// pages/user/index.js
const app = getApp()
let Api = app.Api
Page({
  data: {
    rawData_avatarUrl:"",
    rawData_nickName:"",
    token:{},
    // 被收藏的商品的数量
    collectNums:0
  },
  user_store_btn(e){
    const token=wx.getStorageSync("token");
    if(token){
      //已经登录则直接跳转到店铺页面
      console.log(token)
     app.get(Api.verifyStore,{token:token})
      .then(res=>{
        console.log(res.data)
        if(res.data==1){
          //跳转到店铺页面
          console.log("店铺")
          wx.reLaunch({
            url:"/pages/myshop/myshop"
          })
        }else{
          //错误提示
    
          console.log("注册")
        }

      })
    }else{
      //没有登录先提示登录
    wx.showToast({
      title: '请先登录',
    })
    }
    console.log(e)
  },

  tomesslist(){
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/messlist/messlist',
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }
  },
  tomyorder(){
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/myorder/myorder',
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }
  },
  tomycollect(){
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/mycollect/mycollect',
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }
  },
  tomyaddress(){
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/myaddress/myaddress',
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }
  },
  tomyaccount(){
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/myaccount/myaccount',
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }
  },
  toadmincustomer(){
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/admincustomer/admincustomer',
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }
  },
  //onShow方法是在每次从小程序的前后台切换时都执行的
  onShow(){
    // console.log("onShow")
    const token=wx.getStorageSync("token");
    if(token){
      // console.log("token+++"+token)
      const rawData_avatarUrl=wx.getStorageSync("rawData_avatarUrl");
      const rawData_nickName=wx.getStorageSync('rawData_nickName');
      // console.log(rawData_avatarUrl)
      //const collect=wx.getStorageSync("collect")||[];
      this.setData({
        rawData_avatarUrl:rawData_avatarUrl,
        rawData_nickName:rawData_nickName,
        token:token
      })
    }else{
      this.setData({
        rawData_avatarUrl:"",
        rawData_nickName:"",
        token:""
      })
    }

   // this.setData({userinfo,collectNums:collect.length});
      
  },

  onLoad(){

  },
  onReady(){
 
  },
  onHide(){
   
  },
  onUnload(){
    
  }
})