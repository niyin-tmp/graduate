// pages/historypro/historypro.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hisArray:[],
    history:[]
  },
  toproinfo(e){
  console.log(e)
   wx.navigateTo({
     url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
   })
  },
  del(e){
  console.log("del")
  let productId=e.currentTarget.dataset.productid
  console.log(productId)
  const token=wx.getStorageSync('token')
  app.get(Api.delhistory,{token:token,productId:productId}).then(res=>{
    console.log(res)
    this.onShow()
  })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const token=wx.getStorageSync("token");
    if(!token){
      console.log("mei")
    }
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    const token=wx.getStorageSync("token");
    if(token){
      app.get(Api.gethistorypro,{token:token}).then(res=>{
        console.log(res)
        this.setData({
          hisArray:res.hisArray
        })
      })
    }else{
      console.log("mei")
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})