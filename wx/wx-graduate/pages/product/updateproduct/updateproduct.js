// pages/product/updateproduct/updateproduct.js
const app = getApp()
let Api = app.Api
var adds={};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lookid:false,
        cas:[],
        //存放精选图片
        arra: [], 
        oldarra: [],
        newarra:[],
        //存放详情图片
        arrb: [], 
        oldarrb:[],
        newarrb:[],
        // 商品是否被收藏
        isCollect:false
  },    
  onPullDownRefresh: function () {
    //调用刷新时将执行的方法
    console.log("下拉")
  this.onLoad();
},
  formSubmit: function(e){
   console.log(e)
   console.log("cas")
   console.log(this.data.cas)
   let ProductId=this.data.cas.ProductId
   let ProductName=e.detail.value.ProductName
   console.log(ProductName)
   console.log(ProductId)


  },
    // 提交修改
    formSubmit: function (e) {  
      console.log("--------------")
      console.log(e)
      adds = e.detail.value;   
      if(this.data.arra.length==0){
       wx.showToast({
         title: '请上传精选图片',
       })
      }else{
        this.upload() 
        
      }
      
    },
    upload: function () {  
      console.log(adds)
      console.log(this.cas.ProductId)
      console.log(this.data.oldarra)
      console.log(this.data.oldarrb)
      console.log(this.data.arra)
      var that = this;  
      // console.log(adds)
      const token=wx.getStorageSync("token");
      let env = App.config.baseApi;
      let oldarra=JSON.stringify(this.data.oldarra)
      let oldarrb=JSON.stringify(this.data.oldarrb)
      let arra=JSON.stringify(this.data.arra)
      let arrb=JSON.stringify(this.data.arrb)

      //由于微信不支持多图片上传，所以先上传基本信息，得到商品ID后再上传对应的精选照片
      app.get(Api.updateProduct,{
        token:token,
        product_id:this.cas.ProductId,
        product_name:adds.ProductName,
        product_number:adds.ProductNum,
        product_peice:adds.ProductPrice,
        product_type:adds.ProductType,
        dproduct_describe:adds.dproduct_describe})
      .then(res=>{
        console.log(res)
        app.get(Api.updateImgProduct,{
          token:token,
          oldarra:oldarra,
          oldarrb:oldarrb,
          arra:arra,
          arrb:arrb
        }).then(res=>{
          
          app.get(Api.getimgid,{
            token:token,
            product_id:this.cas.ProductId
          }).then(res=>{
            // 处理精选照片
            // console.log(res.arr1[0])
            // console.log(this.data.arra)
            for(let i=0;i<this.data.arra.length;i++){
              let re=this.data.arra[i].substr(0,10)
              if(re=="http://tmp"){
                console.log("需要处理")
              }else{
                this.data.arra.splice(i,1) 
                i=i-1;
              }
            }
            console.log(this.data.arra)
            console.log("--------")
            for(let i=0;i<this.data.arra.length;i++){
              console.log( res.arr1[i])
              wx.uploadFile({ 
                url: evn+'/product/v1',  
                filePath: this.data.arra[i],  
                name: 'content', 
               formData: {
                product_id:this.cas.ProductId,
                symbol:res.arr1[i],
                imgtype: "0",
                token:token
               },
               success: function (res) {  
                if (res) { 
                  wx.showToast({  
                    title: '已提交发布！',  
                    duration: 3000  
                  });  
                } 
              }  
               })
            }
           // 处理精选照片结束
           //处理详情照片
           for(let i=0;i<this.data.arrb.length;i++){
            let re=this.data.arrb[i].substr(0,10)
            if(re=="http://tmp"){
              console.log("需要处理")
            }else{
              this.data.arrb.splice(i,1) 
              i=i-1;
            }
          }
          console.log(this.data.arrb)
          console.log("--------")
          for(let i=0;i<this.data.arrb.length;i++){
            console.log( res.arr2[i])
            wx.uploadFile({ 
              url: evn+'/product/v1',  
              filePath: this.data.arrb[i],  
              name: 'content', 
             formData: {
              product_id:this.cas.ProductId,
              symbol:res.arr2[i],
              imgtype: "1",
              token:token
             },
             success: function (res) {  
              if (res) { 
                wx.showToast({  
                  title: '已提交发布！',  
                  duration: 3000  
                });  
              } 
            }  
             })
          }
          //处理详情照片结束


          })
        })
        wx.reLaunch({
          url: '../../myshop/myshop',
        })
       
      })

    },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   // let item=JSON.parse(options.str);
  
    this.cas=JSON.parse(decodeURIComponent(options.pro))
    //console.log(this.cas)
    //console.log(item)
     this.arra=this.cas.productImgurlSift
     console.log(this.arra)
     this.arrb=this.cas.productImgurlDetails
    console.log(this.arrb)
    this.setData({
      cas:this.cas,
      arra:this.arra,
      arrb:this.arrb
    })
  },
  // 选中图片删除图片
  deimga(e){
    let i=e.currentTarget.dataset.item
    console.log(i)

    this.data.arra.splice(e.currentTarget.dataset.index,1)
  
    this.setData({ 
      oldarra:this.data.oldarra.concat(i),
      arra: this.data.arra
    }) 
    console.log(this.data.oldarrb) 
    }
    ,
    deimgb(e){
      let i=e.currentTarget.dataset.item
      this.data.arrb.splice(e.currentTarget.dataset.index,1)
      this.setData({  
        oldarrb:this.data.oldarrb.concat(i),
        arrb: this.data.arrb
      })  
      }
      ,
      // 上传精选轮播图片
  upimga: function () {  
    var that = this;  
    if (this.data.arra.length<5){  
    wx.chooseImage({  
      sizeType: ['original', 'compressed'],  
      success: function (res) {  
        //console.log(res.tempFilePaths)
        that.setData({
          //newarra:  that.data.newarra.concat(res.tempFilePaths),
          arra: that.data.arra.concat(res.tempFilePaths)  
        })  
        
      }  
    })  
    }else{  
      wx.showToast({  
        title: '最多上传五张图片',  
        icon: 'loading',  
        duration: 3000  
      });  
    }  
  }, 
// 上传精详情页面图片
upimgb: function () {  
  var that = this;  
  if (this.data.arrb.length<5){  
  wx.chooseImage({  
    sizeType: ['original', 'compressed'],  
    success: function (res) {  
      //console.log(res.tempFilePaths)
      that.setData({  
        arrb: that.data.arrb.concat(res.tempFilePaths)  
      })  
      // console.log(that.data.img_arr)
    }  
  })  
  }else{  
    wx.showToast({  
      title: '最多上传五张图片',  
      icon: 'loading',  
      duration: 3000  
    });  
  }  
}, 
deletepro(e){
console.log(e)
console.log(this.cas)
const token=wx.getStorageSync("token");
app.get(Api.deleteProduct,{
token:token,
product_id:this.cas.ProductId
}).then(res=>{
console.log(res)
wx.redirectTo({
  url: '../../myshop/myshop',
})
})
},
looking(){
console.log("lll")
console.log(this.data.lookid)
if(this.data.lookid){
  this.setData({
    lookid:false
  })
}else{
  this.setData({
    lookid:true
  })
}
},
handleCollect(){
  this.setData({
    isCollect:true
  })
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        mask: true
      });
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})