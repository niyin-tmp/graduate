//index.js
//获取应用实例
const app = getApp()
let Api = app.Api
Page({
  data: {
    // 轮播图数组
    swiperList: [],
    // 导航 数组
    NavList:[],
    //商品数据
    proList:[]
  },
  onLoad: function (options) {
    //获取轮播图
    this.getSwiperList();
   //获取导航栏 
    this.getNavList();
    this.getpro();
  },
  onReady: function () {
  },
  onReachBottom(){
   console.log("到底了")
  },
  //下拉动作
onPullDownRefresh(){

//定时器,1秒
setTimeout(()=>{
  console.log("下拉了")
  this.getSwiperList();
  wx.stopPullDownRefresh()
},1000)
},
//获取轮播图
  getSwiperList(){
    //console.log("获取轮播图")
    app.get(Api.getSwiperList).then(res=>{
    //  console.log(res)
      this.setData({
        swiperList: res
      })
    })
  },
  //获取导航栏
  getNavList(){
   // console.log("获取导航栏")
    app.get(Api.getNavList).then(res=>{
    //console.log(res)
      this.setData({
        NavList:res
      })
  })
  },
  //获取商品
  getpro(){
    app.get(Api.getpro).then(res=>{
     //console.log(res)
     this.setData({
      proList:res
     })
    })
  },
  navproinfo(e){
   // console.log(e)
   const token=wx.getStorageSync("token");
   if(token){
    wx.navigateTo({
      url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
    })
   }else{
    wx.showToast({
      title: '请先登录',
    })
   }

  },
  proinfo(e){
  //console.log(e)
  const token=wx.getStorageSync("token");
  if(token){
    wx.navigateTo({
      url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
    })
  }else{
    wx.showToast({
      title: '请先登录',
    })
  }

  }
})