// pages/category/category.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categorylist:[],
    checklist:[],
    productTypeList:[]
  },
  toproductType(e){
  console.log(e)
  let categorylist=this.data.categorylist;
  let type=e.currentTarget.dataset.producttype
  let checklist=[];
  console.log(categorylist.length)
  for(let i=0;i<categorylist.length;i++){
   // console.log(categorylist[i].proinfo)
    if(categorylist[i].proinfo.productType==type){
      console.log(categorylist[i].proinfo)
      checklist.push(categorylist[i])
    }
  }
  console.log(checklist)
  this.setData({
    checklist:checklist
  })
  },
  proinfo(e){
  console.log(e)
  const token=wx.getStorageSync("token");
  if(token){
    wx.navigateTo({
      url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
    })
  }else{
    wx.showToast({
      title: '请先登录',
    })
  }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  app.get(Api.getCategoryPro).then(res=>{
    console.log(res.array)
    this.setData({
      categorylist:res.array,
      productTypeList:res.productType,
      checklist:res.array
    })
  })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})