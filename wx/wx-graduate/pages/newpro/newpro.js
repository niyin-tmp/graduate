// pages/newpro/newpro.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newlist:[]
  },
  proinfo(e){
    console.log(e)
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.get(Api.getnewpro).then(res=>{
      console.log(res)
      this.setData({
        newlist:res
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})