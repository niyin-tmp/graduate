// pages/search/search.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {

    select:false,
    select1:false,
    show:false,//控制下拉列表的显示隐藏，false隐藏、true显示
    show1:false,//控制下拉列表的显示隐藏，false隐藏、true显示
    productTypeList:["无"],
    selectData1:['无','<1km','<2km','<5km','<10km','<20km','<50km','<100km'],//下拉列表的数据
    index:0,//选择的下拉列表下标
    index1:0,//选择的下拉列表下标
    latitude: null,
    longitude: null,
    searchprolist:[]
  },
  bindShowMsg() {
    this.setData({
     select: !this.data.select
    })
   },
   // 点击下拉显示框
 selectTap(){
  this.setData({
   show: !this.data.show
  });
  },
  // 点击下拉列表
  optionTap(e){
  let Index=e.currentTarget.dataset.index;//获取点击的下拉列表的下标
  console.log(this.data.productTypeList[Index])
  
  this.setData({
   index:Index,
   show:!this.data.show
  });
  },
  //--------
  bindShowMsg1() {
    this.setData({
     select1: !this.data.select1
    })
   },
   // 点击下拉显示框
 selectTap1(){
  this.setData({
   show1: !this.data.show1
  });
  },
  dd(){
    console.log(this.data.searchprolist)
  },
  // 点击下拉列表
  optionTap1(e){
    console.log(e)
  let Index1=e.currentTarget.dataset.index1;//获取点击的下拉列表的下标
  this.setData({
   index1:Index1,
   show1:!this.data.show1
  });
  },
formSubmit(e){
console.log(this.data.productTypeList[this.data.index])
let protype=this.data.productTypeList[this.data.index]
console.log(this.data.selectData1[this.data.index1])
let distance=this.data.selectData1[this.data.index1]
console.log(e.detail.value.searchin)
let searchprolist=[]
let inp=e.detail.value.searchin
let latitude=this.data.latitude
let longitude=this.data.longitude
app.get(Api.getsearchpro,{protype:protype,distance:distance,inp:inp,latitude:latitude,longitude:longitude}).then(res=>{
console.log(res)
if(res.msg==0){
  console.log("请输入信息")
}else if(res.msg==1){
  console.log("收到1")
  console.log(res.dataArray[0])
   for(let i=0;i<res.dataArray.length;i++){
     console.log(res.dataArray[i].logo)
     searchprolist.push(res.dataArray[i])
   }
  this.setData({
   searchprolist: searchprolist
  })

}else if(res.msg==2){
console.log("没有你需要的商品哦")
this.setData({
  searchprolist: []
})

}
})
}
,
proinfo(e){
  wx.navigateTo({
    url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    wx.getLocation({
      type: 'wgs84',
      success (res) {
        const latitude=res.latitude
        const longitude=res.longitude
        app.get(Api.getnearby,{latitude:latitude,longitude:longitude})
        .then(res=>{
          console.log(res)
          that.setData({
            prolist:res
          })
        })

        that.setData({
          latitude:res.latitude,
          longitude:res.longitude,
        })

      }
    })
    app.get(Api.getCategoryPro).then(res=>{
      console.log(res.productType)
      let productTypeList=this.data.productTypeList
      productTypeList=productTypeList.concat(res.productType)
      this.setData({
        productTypeList:productTypeList,
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})