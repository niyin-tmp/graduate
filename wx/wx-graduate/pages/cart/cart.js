// pages/cart/cart.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cartlist:[],
    allChecked: false,
    totalPrice: 0,
    totalNum: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    //console.log(options)
    const token=wx.getStorageSync("token");
    if(token){
      app.get(Api.getprocart,{token:token}).then(res=>{
        // console.log(res)
         this.setData({
           cartlist:res
         })
         })
    }else{
      console.log("nono")
      //没有登录先提示登录
    wx.showToast({
      title: '请先登录',
    })
    }

  },
  handeItemChange(e){
    let productId=e.currentTarget.dataset.id;
  
   let arr=this.data.cartlist;
    for(let i=0;i<arr.length;i++){
      if(productId==arr[i].Productinfo.productId){
        //console.log(arr[i].Productinfo.productId)
        arr[i].checked=!arr[i].checked
      }
      
    }
    this.setData({
      cartlist:arr
    })
    this.setcart(arr)
  },
  setcart(cartlist){
    let allChecked = true;
        // 1 总价格 总数量
        let totalPrice = 0;
        let totalNum = 0;
    //console.log(cartlist)
    for(let i=0;i<cartlist.length;i++){
      if(cartlist[i].checked){
        totalPrice += cartlist[i].num * cartlist[i].Productinfo.productPrice;
        totalNum += cartlist[i].num;
        //console.log(cartlist[i].Productinfo.productPrice) 
      }else {
        allChecked = false;
      }
    }
    //console.log(cartlist)
    // 判断数组是否为空
    allChecked = cartlist.length != 0 ? allChecked : false;
    //console.log(totalPrice)
    //console.log(totalNum)
    this.setData({
      cartlist,
      totalPrice, totalNum, allChecked
    });
    wx.setStorageSync("cartlist", cartlist);
  },
  //点击增加和减少数量
  handleItemNumEdit(e) {
    // 1 获取传递过来的参数 
    const { operation, id } = e.currentTarget.dataset;
    // 2 获取购物车数组
    let {cartlist} = this.data;
    // 3 找到需要修改的商品的索引
    //console.log(cartlist)
    for(let i=0;i<cartlist.length;i++){
      if(id==cartlist[i].Productinfo.productId){
       // console.log(cartlist[i].Productinfo.productId)
        if (cartlist[i].num === 1 && operation === -1)
        {
        // 4.1 弹窗提示
        wx.showModal({
          title: '提示',
          content: "确定删除吗",
          success :(res) =>{
           if(res.confirm){
            //连接后台做处理
            const token=wx.getStorageSync("token");
            let productId=cartlist[i].Productinfo.productId
            app.get(Api.deleteproductcart,{token:token,productId:productId}).then(res=>{
              console.log(res)
            })
            //console.log("确定")
            cartlist.splice(i, 1);
            this.setcart(cartlist);
           }
           if(res.cancel){
           // console.log("取消")
          }
          }
        })
      }else{
        //设置值
              // 4  进行修改数量
      cartlist[i].num += operation;
      // 5 设置回缓存和data中
      this.setcart(cartlist);
      }
      }
    } 
  },
  //全选功能
  handleItemAllCheck(){
    let { cartlist, allChecked } = this.data;
    // 2 修改值
    allChecked = !allChecked;
    console.log(allChecked)
    console.log(cartlist)
    for(let i=0;i<cartlist.length;i++){
      cartlist[i].checked=allChecked
     
    }
    
    this.setcart(cartlist);
  },
  handlePay(){
        const {address,totalNum}=this.data;
      // 2 判断用户有没有选购商品
      if(totalNum===0){
        wx.showToast({
          title: '请选择商品',
          icon: 'success',
          duration: 2000,
        })
        return;
      }
      // 3 跳转到 支付页面
        wx.navigateTo({
          url: '/pages/pay/pay'
        });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})