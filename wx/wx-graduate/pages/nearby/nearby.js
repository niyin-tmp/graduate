// pages/nearby/nearby.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    latitude: null,
    longitude: null,
    prolist:[]
  },
  select(){
    let pros=this.data.prolist
    console.log(pros)

  },
  proinfo(e){
    //console.log(e)
    const token=wx.getStorageSync("token");
    if(token){
      wx.navigateTo({
        url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
      })
    }else{
      wx.showToast({
        title: '请先登录',
      })
    }

    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    wx.getLocation({
      type: 'wgs84',
      success (res) {
        const latitude=res.latitude
        const longitude=res.longitude
        app.get(Api.getnearby,{latitude:latitude,longitude:longitude})
        .then(res=>{
          console.log(res)
          that.setData({
            prolist:res
          })
        })

        that.setData({
          latitude:res.latitude,
          longitude:res.longitude,
        })

      }
    })
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})