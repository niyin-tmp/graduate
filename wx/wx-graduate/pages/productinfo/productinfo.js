// pages/productinfo/productinfo.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    productId:"",
    storeid:"",
    addresslist:[],
    productinfo:[],
    //存放精选图片
    arra: [], 
    //存放详情图片
    arrb: [], 
    // 商品是否被收藏
    isCollect:false,
    select:false,
     //当前地址
     address:"",
     //当前收货人
     name:"",
     //收货人手机
     phone:"",
     num:1,
     //支付链接
     payurl:"",
  },

  bindShowMsg() {
    this.setData({
     select: !this.data.select
    })
   },
   handleItemNumEdit(e) {
    const { operation} = e.currentTarget.dataset;
    console.log(operation)
    if(this.data.num===1&&operation===-1){
        // 4.1 弹窗提示
        wx.showModal({
          title: '提示',
          content: "至少要选择1件商品哦",
          success :(res) =>{
            console.log(res)
          }
        })
}else{
  let num =this.data.num+operation;
  this.setData({
    num:num
  })
}
   },
   toshop(){
     console.log(this.data.storeid)
     wx.navigateTo({
      url: '/pages/store/store?storeid='+this.data.storeid
    })
   },
   tokefu(){
    wx.navigateTo({
      url: '/pages/mycustomer/mycustomer?storeid='+this.data.storeid
    })
   },
   mySelect(e) {
    console.log(e)
    var name = e.currentTarget.dataset.name
    var address = e.currentTarget.dataset.address
    this.setData({
     address: address,
     name:name,
     select: false
    })
   },
  //收藏
  handleCollect(){
    const token=wx.getStorageSync("token");

    if(!this.data.isCollect){
      app.get(Api.addcollect,{token:token,productId:this.data.productId}).then(res=>{
        wx.showToast({
          title: "已收藏",
        })
        console.log(res)
        this.setData({
          isCollect:true
        })
      })
    }
    if(this.data.isCollect){
      app.get(Api.deletecollect,{token:token,productId:this.data.productId}).then(res=>{
        wx.showToast({
          title: "取消收藏",
        })
        console.log(res)
        this.setData({
          isCollect:false
        })
      })
    }
  },
  binpay(){
    let that=this;
  console.log(this.data.productinfo)  
  console.log(this.data.arra[0])
  let productinfo=this.data.productinfo;
  const token=wx.getStorageSync("token");
  let name=this.data.name;
  let phone=this.data.phone;
  let address=this.data.address;
  let num=this.data.num;
  app.get(Api.payinfox,{name:name,phone:phone,address:address,num:num,token:token,productinfo:productinfo}).then(res=>{
   console.log(res)
   that.setData({
    payurl:res
    })
  })
  },
//加入购物车
handleCartAdd(){
  const token=wx.getStorageSync("token");
  app.get(Api.addproductcart,{token:token,productId:this.data.productId}).then(res=>{
    wx.showToast({
      title: "加入成功",
    })
  })
},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   // console.log(options.productid)
    let productId=options.productid
    const token=wx.getStorageSync("token");
    //做请求增浏览量
    app.get(Api.addPageviges,{productId:productId}).then(res1=>{
     // console.log(res)
      app.get(Api.getproductinfo,{token:token,productId:productId}).then(
        res2=>{
          console.log(res2)
          console.log(res2.collect)
          if(res2.collect=="1"){
            this.setData({
              isCollect:true
            })
          }
          this.setData({
            productinfo:res2.productinfo,
            storeid:res2.productinfo.storeid,
            arra:res2.arra,
            arrb:res2.arrb,
          })
        }
      )
    })


    this.setData({
      productId:productId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const token=wx.getStorageSync("token");
    app.get(Api.getaddress,{token:token}).then(res=>{
      console.log(res)
      for(let i=0;i<res.length;i++){
        //console.log(res[i].address)
        if(res[i].status=='1'){
          console.log(res[i].address)
          this.setData({
            name:res[i].name,
            address:res[i].address,
            phone:res[i].phone
          })
        }
        }
      this.setData({
        addresslist:res
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})