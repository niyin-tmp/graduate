
const app = getApp()
let Api = app.Api
// pages/myaccount/myaccount.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_name:"",
    phone:""
  },
  addphone(){
    wx.navigateTo({    //保留当前页面，跳转到应用内的某个页面（最多打开5个页面，之后按钮就没有响应的）
      url:"/pages/addphone/addphone"
  })
  },
  exit(){
    console.log("退出账号")
    wx.clearStorageSync()
    wx.switchTab({
      url: '/pages/user/user',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const user_name=wx.getStorageSync('rawData_nickName')
    const token=wx.getStorageSync('token')
    console.log(token)
    //获取手机号码
    app.get(Api.getPhone,{token:token})
    .then(res=>{
      console.log(res)
      if(res=="error"){
        this.setData({
          phone:""
        })
      }else{
        const phone=res.phone
        this.setData({
          phone:phone
        })
      }
      
    })
    .catch()
    this.setData({
      user_name:user_name,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})