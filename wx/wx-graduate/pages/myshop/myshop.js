// pages/myshop/myshop.js
const app = getApp()
let Api = app.Api
var adds={};
var interval=null;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //弹框
    hiddenmodalput:true,  
    //初始显示
    click_id:"customer",
    //存放精选图片
    img_arra: [], 
    //存放详情图片
    img_arrb: [], 
    // imgurls: [],
    //存放返回的产品信息
    cas:[], 
    formdata: '', 
    pronamevalue:null,
    pronvalue:null,
    propvalue:null,
    protype:null,
    latitude:null,
    longitude:null,
    address:null,
    orderlist:[],
    cunum:"22",
    name:"雨夜听风",
    userhistorylist:[],
    s_address:"",
    s_id:"",
    s_name:"",
    s_num:"",
    s_numprice:"",
    s_ordernum:"",
    s_phone:"",
    s_productId:"",
    s_proname:"",
    status:""

  },
  tohis(e){
    console.log(e)
    const token=wx.getStorageSync("token");
    let useropenid=e.currentTarget.dataset.useropenid
    console.log(useropenid)
    app.get(Api.setemp,{token:token,useropenid:useropenid}).then(res=>{
      console.log(res)
      this.btns()
       wx.navigateTo({
         url: '/pages/storecustomer/storecustomer?useropenid='+useropenid+'&&username='+this.data.name
       })
    })
  },
  btns(){
    let that=this
    const token=wx.getStorageSync("token");
    let env = App.config.baseApi;
    wx.request({
      url:env+"/ChatHistory/gethistorynum",
      data:{"token":token},
      header:{
         // "Content-Type":"application/json"
      },
      success:function(res){
         // console.log(res.data.data)
          that.setData({
            userhistorylist:res.data.data
          })
        
      },
      fail:function(err){
          console.log(err)
      }

  })
  },
  setCountdown: function () {
    //因为interval函数中的this域不一样了，需要把页面的this提取出来
    var that = this;  
    const token=wx.getStorageSync("token");
    let storeid=this.data.storeid
    let env = App.config.baseApi;
    interval = setInterval(function () {
      //常规请求
      that.btns()
    }, 10000)
  },
  // 产品发布
  formSubmit: function (e) {  
    adds = e.detail.value;   
    if(this.data.img_arra.length==0){
     wx.showToast({
       title: '请上传精选图片',
     })
    }else{
      this.upload() 
    }
    
  }
  ,
  // 产品发布
  upload: function () {  
    var that = this;  
    let env = App.config.baseApi;
    // console.log(adds)
    const token=wx.getStorageSync("token");
    //由于微信不支持多图片上传，所以先上传基本信息，得到商品ID后再上传对应的精选照片
    app.get(Api.uploadProduct,{
      token:token,
      product_id:Date.parse(new Date()),
      product_name:adds.product_name,
      product_number:adds.product_number,
      product_peice:adds.product_peice,
      product_type:adds.product_type,
      dproduct_describe:adds.dproduct_describe})
    .then(res=>{
      console.log(res)
      let roid=res
      for (let i=0; i < this.data.img_arra.length; i++) { 
        wx.uploadFile({  
          url: env+'/product/v1',  
          filePath: that.data.img_arra[i],  
          name: 'content',  
          formData: {
            product_id:roid,
            symbol:i,
            imgtype: "0",
            token:token
          },   
          success: function (res) {  
            // console.log(JSON.parse(res.data)) 
            // let arr=JSON.parse(res.data)
            // console.log(arr.data.imgurl)
            // that.setData({  
            //   imgurls: that.data.imgurls.concat(arr.data.imgurl)
            // })
            if (res) { 
              wx.showToast({  
                title: '已提交发布！',  
                duration: 3000  
              });  
            } 
          }  
        })
        if(i==this.data.img_arra.length-1){
          this.setData({
            img_arra: []
            //存放详情图片
 
          })
        }
           //console.log(that.data.imgurls)      
      }  
      for (let i=0; i < this.data.img_arrb.length; i++) { 
        wx.uploadFile({  
          url: env+'/product/v1',  
          filePath: that.data.img_arrb[i],  
          name: 'content',  
          formData: {
            product_id:roid,
            symbol:i,
            imgtype: "1",
            token:token
          },   
          success: function (res) {  
            // console.log(JSON.parse(res.data)) 
            // let arr=JSON.parse(res.data)
            // console.log(arr.data.imgurl)
            // that.setData({  
            //   imgurls: that.data.imgurls.concat(arr.data.imgurl)
            // })
            if (res) { 
              wx.showToast({  
                title: '已提交发布！',  
                duration: 3000  
              });  
            } 
          }  
        })  
       if(i==this.data.img_arrb.length-1){
        this.setData({
          img_arrb: []
        })
       }
           //console.log(that.data.imgurls)      
      }  


    })
     
      this.setData({  
        formdata: '' ,
        pronamevalue:"",
        pronvalue:"",
        propvalue:"",
        protype:""
      })  
  },  
  // 上传精选轮播图片
  upimga: function () {  
    var that = this;  
    if (this.data.img_arra.length<5){  
    wx.chooseImage({  
      sizeType: ['original', 'compressed'],  
      success: function (res) {  
        //console.log(res.tempFilePaths)
        that.setData({  
          img_arra: that.data.img_arra.concat(res.tempFilePaths)  
        })  
        // console.log(that.data.img_arr)
      }  
    })  
    }else{  
      wx.showToast({  
        title: '最多上传五张图片',  
        icon: 'loading',  
        duration: 3000  
      });  
    }  
  }, 
// 上传精详情页面图片
upimgb: function () {  
  var that = this;  
  if (this.data.img_arrb.length<5){  
  wx.chooseImage({  
    sizeType: ['original', 'compressed'],  
    success: function (res) {  
      //console.log(res.tempFilePaths)
      that.setData({  
        img_arrb: that.data.img_arrb.concat(res.tempFilePaths)  
      })  
      // console.log(that.data.img_arr)
    }  
  })  
  }else{  
    wx.showToast({  
      title: '最多上传五张图片',  
      icon: 'loading',  
      duration: 3000  
    });  
  }  
}, 

  // 选中图片删除图片
deimga(e){
this.data.img_arra.splice(e.currentTarget.dataset.index,1)
this.setData({  
  img_arra: this.data.img_arra
})  
}
,
deimgb(e){
  this.data.img_arrb.splice(e.currentTarget.dataset.index,1)
  this.setData({  
    img_arrb: this.data.img_arrb
  })  
  }
  ,

  //管理产品
  manage(){
    const click_id="manage";
    console.log(click_id);
    const token=wx.getStorageSync("token");
    app.get(Api.manageProduct,{
      token:token
    })
    .then(res=>{
      console.log(res.Productlist)
      this.cas=res.Productlist;
      console.log(this.cas)
      //console.log(this.cas[0].productImgurlSift[0])
      this.setData({
        cas:this.cas
      })
    })


    this.setData({
      click_id:click_id
    })
  },
  //修改产品信息
  updateproduct(e){
    console.log(e)
    let pro=JSON.stringify(e.currentTarget.dataset.item)
   // console.log(pro)
    wx.navigateTo({
      //如果对象的参数或数组的元素中遇到地址，地址中包括?、&这些特殊符号时，
      //对象/数组先要通过JSON.stringify转化为字符串再通过encodeURIComponent编码，
      //接收时，先通过decodeURIComponent解码再通过JSON.parse转换为JSON格式的对象/数组
      url:"../product/updateproduct/updateproduct?pro="+encodeURIComponent(pro)
      ,
      success:function(res){

      }
    })
  },
  // 点击发布产品时触发
  release(){
    const click_id="release";
    console.log(click_id);
    this.setData({
      click_id:click_id
    })
  },
//待处理订单
pend(){
  const click_id="pend";
  console.log(click_id);
  const token=wx.getStorageSync("token");
  app.get(Api.getorderlist,{token:token}).then(res=>{
  console.log(res)
  this.setData({
    orderlist:res
  })
  })
  this.setData({
    click_id:click_id
  })
},
shipped(){
  const click_id="shipped";
  console.log(click_id);
  const token=wx.getStorageSync("token");
  app.get(Api.getorderlist,{token:token}).then(res=>{
  console.log(res)
  this.setData({
    orderlist:res
  })
  })
  this.setData({
    click_id:click_id
  })
},
done(){
  const click_id="done";
  console.log(click_id);
  const token=wx.getStorageSync("token");
  app.get(Api.getorderlist,{token:token}).then(res=>{
  console.log(res)
  this.setData({
    orderlist:res
  })
  })
  this.setData({
    click_id:click_id
  })
}
,
address(){
  const click_id="address";
  const token=wx.getStorageSync("token");
  app.get(Api.getstoreaddress,{token:token}).then(res=>{
    console.log(res)
     let latitude=res.latitude
     let longitude=res.longitude
     let address=res.address
     this.setData({
       address:address,
       latitude:latitude,
       longitude:longitude
     })
  })
  console.log(click_id);
  this.setData({
    click_id:click_id
  })
}
,
upaddress(){
  wx.navigateTo({    //保留当前页面，跳转到应用内的某个页面（最多打开5个页面，之后按钮就没有响应的）
    url:"/pages/address/address"
})
}
,
customer(){
    const click_id="customer";
    console.log(click_id);
    this.setData({
      click_id:click_id
    })
  },
//发货
topro(e){
console.log(e)
const token=wx.getStorageSync("token");
let ordernum=e.currentTarget.dataset.ordernum
app.get(Api.storesetorder,{token:token,ordernum:ordernum}).then(res=>{
  console.log(res)
})

this.shipped()
},
toinfopro(e){
  console.log(e.currentTarget.dataset.id)
  let id=e.currentTarget.dataset.id
  for(let i=0;i<this.data.orderlist.length;i++){
   // console.log(this.data.orderlist[i].id)
    if(id==this.data.orderlist[i].id){
      console.log(this.data.orderlist[i])
      this.setData({
        s_address:this.data.orderlist[i].address,
        s_id:this.data.orderlist[i].id,
        s_name:this.data.orderlist[i].name,
        s_num:this.data.orderlist[i].num,
        s_numprice:this.data.orderlist[i].numprice,
        s_ordernum:this.data.orderlist[i].ordernum,
        s_phone:this.data.orderlist[i].phone,
        s_productId:this.data.orderlist[i].productId,
        s_proname:this.data.orderlist[i].proname,
        status:this.data.orderlist[i].status
      });
    }
  }
  console.log("查看详情")
  console.log(this.data.orderlist)
  this.setData({  
    hiddenmodalput: false  
    });  
    },
        //取消按钮  
        cancel: function(){  
          this.setData({  
              hiddenmodalput: true  
          });  
      },  
      //确认  
      confirm: function(){  
          this.setData({  
              hiddenmodalput: true  
          })  
      } , 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.btns()
    this.setCountdown()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   // console.log(this.data.click_id)
 
    if(this.data.click_id=='manage'){
      this.manage()
    }
    this.btns()
    this.setCountdown()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    clearInterval(interval);
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(interval);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})