// pages/mycollect/mycollect.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
  productlist:[],
  allChecked: false,
  totalNum: 0
  },
  handeItemChange(e){
console.log(e)
let productId=e.currentTarget.dataset.id;
let arr=this.data.productlist;
for(let i=0;i<arr.length;i++){
  if(productId==arr[i].product.productId){
    //console.log(arr[i].Productinfo.productId)
    arr[i].checked=!arr[i].checked
  }
  
}
this.setData({
  productlist:arr
})
this.setcollect(arr)
  },
  setcollect(productlist){
    let allChecked = true;
    let totalNum = 0;
    for(let i=0;i<productlist.length;i++){
      if(productlist[i].checked){
        totalNum += productlist[i].num;
      }else {
        allChecked = false;
      }
    }
    allChecked = productlist.length != 0 ? allChecked : false;
    this.setData({
      productlist,
      totalNum, allChecked
    });
    wx.setStorageSync("productlist", productlist);
  },
    //全选功能
    handleItemAllCheck(){
      let { productlist, allChecked } = this.data;
      // 2 修改值
      allChecked = !allChecked;
      //console.log(allChecked)
      //console.log(productlist)
      for(let i=0;i<productlist.length;i++){
        productlist[i].checked=allChecked
       
      }
      
      this.setcollect(productlist);
    },
    toproinfo(){
     console.log("toinfi")
    },
    deletecollect(){
      const {totalNum}=this.data;
      let pro=this.data.productlist
      //console.log(pro)
      if(totalNum===0){
        wx.showModal({
          title: '提示',
          content: "你还没有选择商品呢",
          success :(res) =>{

          }})
        return ;
      }
      const token=wx.getStorageSync("token");
      for(let i=0;i<pro.length;i++){
        if(pro[i].checked){
          console.log(pro[i].product.productId)
          let productId=pro[i].product.productId
         app.get(Api.deletecollect,{token:token,productId:productId}).then(res=>{
           console.log(res)
           if(res=="ok"){
            wx.showToast({  
              title: '删除成功',  
              icon: 'loading',  
              duration: 2000  
            });  
           }
          

         })
        } 
      }
      this.onShow()
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  const token=wx.getStorageSync("token");
  
  app.get(Api.getcollect,{token:token}).then(res=>{
  console.log(res)
  this.setData({
    productlist:res
  })
})
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})