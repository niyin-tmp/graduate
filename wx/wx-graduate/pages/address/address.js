// pages/address/address.js
const app = getApp()
let Api = app.Api
var QQMapWX = require('../../utils/map/qqmap-wx-jssdk.js');
var qqmapsdk;
const key = 'xxx'; //使用在腾讯位置服务申请的key
const referer = '腾讯位置服务地图选点'; //调用插件的app的名称

const category = '生活服务,娱乐休闲';
const chooseLocation = requirePlugin('chooseLocation');
Page({

  /**
   * 页面的初始数据
   */
  data: {
      address:"",
      latitude: null,
      longitude: null
  },
  setaddress(){
    console.log(this.data.latitude)
    console.log(this.data.longitude)
    const token=wx.getStorageSync("token");
    let latitude=this.data.latitude
    let longitude=this.data.longitude
    let address=this.data.address
    app.get(Api.setstoreaddress,{token:token,latitude:latitude,longitude:longitude,address:address}).then(res=>{
      console.log(res)
      wx.showToast({
        title: '上传成功',
        icon: 'success',
        duration: 2000
      })
    })
  },
  mapopen(){
    console.log(this.data.latitude)
    let latitude=this.data.latitude
    let longitude=this.data.longitude
    wx.openLocation({
      latitude,
      longitude,
      scale: 18
    })
  },
  mapview(){
    wx.getLocation({
      type: 'wgs84',
      success (res) {
        const latitude = res.latitude
        const longitude = res.longitude
        const speed = res.speed
        const accuracy = res.accuracy
        const location = JSON.stringify({
          latitude: latitude,
          longitude: longitude
        });
        wx.navigateTo({
          url: 'plugin://chooseLocation/index?key=' + key + '&referer=' + referer + '&location=' + location + '&category=' + category
          });
      }
     })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const token=wx.getStorageSync("token");
    app.get(Api.getstoreaddress,{token:token}).then(res=>{
      console.log(res)
       let latitude=res.latitude
       let longitude=res.longitude
       let address=res.address
       this.setData({
         address:address,
         latitude:latitude,
         longitude:longitude
       })
    })
    qqmapsdk = new QQMapWX({
      key: 'xxx'
     });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const location = chooseLocation.getLocation();
    if(location!=null){
      console.log(location)
      const latitude = location.latitude
      const longitude = location.longitude
      const address = location.address
      this.setData({
        address:address,
        latitude:latitude,
        longitude:longitude
      })
      
    }



// 调用接口
// qqmapsdk.search({
//   keyword: '酒店',
//   success: function (res) {
//       console.log(res);
//   },
//   fail: function (res) {
//       console.log(res);
//   },
// complete: function (res) {
//   console.log(res);
// }
//});
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    chooseLocation.setLocation(null);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})