// pages/test/test.js
// pages/pay/pay.js
const app = getApp()
let Api = app.Api
Page({
 /**
  * 页面的初始数据
  */
 data: {
  select:false,
  name:'',
  grade_name:'',
  grades: []
 },  
 onShow: function () {
  let cartlist = wx.getStorageSync("cartlist") || [];
  const token=wx.getStorageSync("token");
  console.log(cartlist)
  app.get(Api.getaddress,{token:token}).then(res=>{
    console.log(res)
    for(let i=0;i<res.length;i++){
    //console.log(res[i].address)
    if(res[i].status=='1'){
      console.log(res[i].address)
      this.setData({
        name:res[i].name,
        grade_name:res[i].address
      })
    }
    }
    this.setData({
      grades:res
    })
  })
},
 /**
 * 点击下拉框
 */
 bindShowMsg() {
  this.setData({
   select: !this.data.select
  })
 },
/**
 * 已选下拉框
 */
 mySelect(e) {
  console.log(e)
  var name = e.currentTarget.dataset.name
  var grade_name = e.currentTarget.dataset.grade_name
  this.setData({
   grade_name: grade_name,
   name:name,
   select: false
  })
 }
})