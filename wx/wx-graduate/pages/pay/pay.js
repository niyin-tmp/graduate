// pages/pay/pay.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //支付链接
    payurl:"",
    //地址数组
    addresslist:[],
    //已选商品数组
    prolist:[],
    //当前地址
    address:"",
    //当前收货人
    name:"",
    //收货人手机
    phone:"",
    //是否显示可选地址
    select:false,
    //allprice
    allprice:0
  },
 /**
 * 点击下拉框
 */
bindShowMsg() {
  this.setData({
   select: !this.data.select
  })
 },
 mySelect(e) {
  console.log(e)
  var name = e.currentTarget.dataset.name
  var address = e.currentTarget.dataset.address
  this.setData({
   address: address,
   name:name,
   select: false
  })
 },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let prolist = wx.getStorageSync("cartlist") || [];
    const token=wx.getStorageSync("token");
    console.log(prolist)
    let allprice=0;
    for(let j=0;j<prolist.length;j++){
      //console.log(cartlist[j])
      if(prolist[j].checked){
        console.log(prolist[j])
        allprice+=prolist[j].Productinfo.productPrice*prolist[j].num
        console.log(allprice)
        this.setData({
          allprice:allprice,
          prolist:this.data.prolist.concat(prolist[j])
        })
      }
    }
    app.get(Api.getaddress,{token:token}).then(res=>{
      console.log(res)
      for(let i=0;i<res.length;i++){
        //console.log(res[i].address)
        if(res[i].status=='1'){
          console.log(res[i].address)
          this.setData({
            name:res[i].name,
            address:res[i].address,
            phone:res[i].phone
          })
        }
        }
      this.setData({
        addresslist:res
      })
    })
  },
  payinfo(){
    var that = this;
    console.log("ooo")
    const token=wx.getStorageSync("token");
    let cartlist = wx.getStorageSync("cartlist") || [];
    let env = App.config.baseApi;
    let name=this.data.name;
    let phone=this.data.phone;
    let address=this.data.address;
    wx.request({
      url : env+"/pay/payzf",
      method: "POST",
      data: {
        name:name,
        phone:phone,
        address:address,
        token : token,
        cartlist:JSON.stringify(cartlist)
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log(res.data.data);
        that.setData({
          payurl:res.data.data
        })
        wx.showToast({
          title: '请求成功',
          icon: 'success',
          duration: 2000
        })
      },
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.setData({
      payurl:""
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})