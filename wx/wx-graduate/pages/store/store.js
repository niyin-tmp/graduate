// pages/store/store.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeid:"",
    storename:"",
    latitude:"",
    longitude:"",
    prolist:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  console.log(options)
  let storeid=options.storeid
  const token=wx.getStorageSync("token");
  app.get(Api.getstoreinfo,{token:token,storeid:storeid}).then(res=>{
  console.log(res)
  this.setData({
    prolist:res.proArray,
    storename:res.storeinfo.storeType,
    latitude:res.storeinfo.latitude,
    longitude:res.storeinfo.longitude
  })
  })
  this.setData({
    storeid:options.storeid
  })
  },
  toaddress(){
    console.log(this.data.latitude)
    let latitude=this.data.latitude
    let longitude=this.data.longitude
    wx.openLocation({
      latitude,
      longitude,
      scale: 18
    })
  },
  tocustomer(){
    wx.navigateTo({
      url: '/pages/mycustomer/mycustomer?storeid='+this.data.storeid
    })
  },
  proinfo(e){
    console.log(e)
    wx.navigateTo({
      url: '/pages/productinfo/productinfo?productid='+e.currentTarget.dataset.productid
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})