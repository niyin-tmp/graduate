// pages/myorder/myorder.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
  prolist:[],
  sel:"0",
  ispay:false,
  payurl:"",
  hiddenmodalput:true,
  t_proname:"",
  t_ordernum:"",
  t_num:"",
  t_numprice:"",
  t_name:"",
  t_phone:"",
  t_address:""
  },
  nopay(){
  this.setData({
  sel:"0"
})
  },
  noship(){
    this.setData({
      sel:"1"
    })
  },
  proname(e){

  console.log("查看详情")
  console.log(e)

      this.setData({
        // s_address:this.data.orderlist[i].address,
        t_proname:e.currentTarget.dataset.proname,
        t_ordernum:e.currentTarget.dataset.ordernum,
        t_num:e.currentTarget.dataset.num,
        t_numprice:e.currentTarget.dataset.numprice,
        t_name:e.currentTarget.dataset.name,
        t_phone:e.currentTarget.dataset.phone,
        t_address:e.currentTarget.dataset.address
      });
      this.hiddenmodalput=false
  //console.log("查看详情")
  this.setData({  
    hiddenmodalput: false  
    });  
    },
        //取消按钮  
        cancel: function(){  
          this.setData({  
              hiddenmodalput: true  
          });  
      },  
      //确认  
      confirm: function(){  
          this.setData({  
              hiddenmodalput: true  
          })  
  },
  ship(){
    this.setData({
      sel:"2"
    })
  },

  finish(){
    this.setData({
      sel:"3"
    })
  },
  topay(e){
    console.log(e.currentTarget.dataset)
    const token=wx.getStorageSync("token");
    let orderNo=e.currentTarget.dataset.orderno
    let numprice=e.currentTarget.dataset.numprice
    app.get(Api.gettopay,{token:token,orderNo:orderNo,numprice:numprice}).then(res=>{
     console.log(res)
     this.setData({
      ispay:true,
      payurl:res
     })
    })
  },
  //确认收获
  ok(e){
    console.log("确认收货")
const token=wx.getStorageSync("token");
let ordernum=e.currentTarget.dataset.ordernum
app.get(Api.okorder,{token:token,ordernum:ordernum}).then(res=>{
  console.log(res)
})
this.onShow()
  },
  //删除
  orderdelete(e){
const token=wx.getStorageSync("token");
let ordernum=e.currentTarget.dataset.ordernum
app.get(Api.orderdelete,{token:token,ordernum:ordernum}).then(res=>{
  console.log(res)
})
this.onShow()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      payurl:""
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const token=wx.getStorageSync("token");
    app.get(Api.getOrders,{token:token}).then(res=>{
      console.log(res)
      this.setData({
        prolist:res,
        payurl:""
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})