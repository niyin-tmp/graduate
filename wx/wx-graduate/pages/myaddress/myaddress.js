// pages/myaddress/myaddress.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addresslist:[],
    addid:false
  },
  toadds(e){
    console.log(e)
    let id=e.currentTarget.dataset.id;
    const token=wx.getStorageSync("token");
    app.get(Api.setmoaddress,{token:token,id:id}).then(res=>{
      console.log(res)
      this.onShow()
    })
  },
  formsubmit(e){
    const token=wx.getStorageSync("token");
    let name=e.detail.value.name
    let phone=e.detail.value.phone
    let address=e.detail.value.newaddress
    console.log(e.detail.value.newaddress)
    app.get(Api.setaddress,{token:token,name:name,phone:phone,address:address}).then(res=>{
      console.log(res)
      this.setData({
        addid:false
      })
      this.getaddress()
    })
  },
  getaddress(){
    const token=wx.getStorageSync("token");
    app.get(Api.getaddress,{token:token}).then(res=>{
      console.log(res)
      
      this.setData({
        addresslist:res
      })
    })
  },
  delete(e){
    console.log(e.currentTarget.dataset.id)
    let id=e.currentTarget.dataset.id;
    const token=wx.getStorageSync("token");
    app.get(Api.deleteaddress,{token:token,id:id}).then(res=>{
      console.log(res)
      this.onShow()
    })
  },
  add(){
    let addid=!this.data.addid
    this.setData({
      addid:addid
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getaddress()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})