const app = getApp()
let Api = app.Api
let store = require("../../utils/store.js")
// pages/login/login.js
Page({
  data: {
    code:""
  },
  onShow(){
    wx.login({
      success:login_res=>{
        console.log(login_res)
        this.setData({
          code:login_res.code
        })
      }
    })
  },
  getUserProfile(e){
      wx.getUserProfile({
        desc: '正在获取', //不写不弹提示框
        success:info_res=>{
        if(this.data.code){
          console.log(info_res)
          app.get(Api.WxLogin,{code:this.data.code,rawData:info_res.rawData})
          .then(login_res =>{
            wx.setStorageSync("token",login_res.token);
            //const {userInfo}=info_res;
            wx.setStorageSync("rawData_avatarUrl",login_res.avatarUrl);
            wx.setStorageSync("rawData_nickName",login_res.nickName);
            wx.reLaunch({
              url: '/pages/user/user'
            })
          }).catch(err =>{
            console.log(err.message)
          })
        }
      },      
      fail: function (err) {
        console.log("获取失败: ", err)
      }
      })

  },
phonelogin(){
  wx.navigateTo({
    url:"/pages/phonelogin/phonelogin"
  })
},
maillogin(){
  wx.navigateTo({
    url:"/pages/maillogin/maillogin"
  })
}
})