// pages/messlist/messlist.js
const app = getApp()
let Api = app.Api
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[]
  },
  tocustomer(e){
    console.log(e.currentTarget.dataset.storeid)
    let storeid=e.currentTarget.dataset.storeid
    const token=wx.getStorageSync("token");
    app.get(Api.usersetemp,{token:token,storeid:storeid}).then(res=>{
      console.log(res)
       wx.navigateTo({
         url: '/pages/mycustomer/mycustomer?storeid='+storeid
       })
    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const token=wx.getStorageSync("token");
    app.get(Api.getstorelist,{token:token}).then(res=>{
      console.log(res)
      this.setData({
        list:res
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})