// pages/mycustomer/mycustomer.js
const app = getApp()
let Api = app.Api
var interval=null;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    storeid:"1",
    id:"",
    storeName:"惠民小店",
    toid:"toid",
    list:[],
    inputinfo:"",
    rawData_avatarUrl:""
  },

  formSubmit(e){
    let that=this
    console.log(e.detail.value.in)
    let content=e.detail.value.in
    let storeid=this.data.storeid
    const token=wx.getStorageSync("token");
    let env = App.config.baseApi;
    if(content!=""){
    console.log("qingqiu")
    }
    let contype="0"
    app.get(Api.setchathistory,{token:token,content:content,storeid:storeid,contype:contype}).then(res=>{
      console.log(res)
      wx.request({
        url:env+"/ChatHistory/getchathistory",
        data:{"token":token,"storeid":storeid},
        header:{
           // "Content-Type":"application/json"
        },
        success:function(res){
            console.log(res.data.data.length)
            let id="";
            for(let i=0;i<res.data.data.length;i++){
              id=res.data.data[i].id
              console.log(res.data.data[i].id)
             }
             that.setData({
              id:"a"+id,
              list:res.data.data
             })
            that.setData({
              list:res.data.data
             })
        },
        fail:function(err){
            console.log(err)
        }
  
    })
    })
    this.setData({
      inputinfo:""
    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that=this
    console.log(options)
    let storeid=options.storeid
    const token=wx.getStorageSync("token");
    const rawData_avatarUrl=wx.getStorageSync("rawData_avatarUrl");
   app.get(Api.getchathistory,{token:token,storeid:storeid}).then(res=>{
     console.log(res)
     let id="";
     for(let i=0;i<res.length;i++){
      id=res[i].id
     }
     that.setData({
      rawData_avatarUrl:rawData_avatarUrl,
      id:"a"+id,
      list:res
     })
     console.log(id)
   })
   console.log("aaaa")
   this.setCountdown();
    // this.setData({
    //   storeid:options.storeid
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */

  onShow: function () {


  },
// 设置每秒一次。
setCountdown: function () {
  //因为interval函数中的this域不一样了，需要把页面的this提取出来
  var that = this;  
  const token=wx.getStorageSync("token");
  let storeid=this.data.storeid
  let env = App.config.baseApi;
  interval = setInterval(function () {
    //常规请求
     wx.request({
      url:env+"/ChatHistory/getchathistory",
      data:{"token":token,"storeid":storeid},
      header:{
         // "Content-Type":"application/json"
      },
      success:function(res){
          console.log(res.data)
          that.setData({
            list:res.data.data
           })
      },
      fail:function(err){
          console.log(err)
      }

  })


  }, 5000)
},
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    clearInterval(interval);
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(interval);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})