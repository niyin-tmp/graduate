import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
const Home = () => import(/* webpackChunkName: "Login_Home_Welcome" */ '../components/Home.vue')
const Welcome = () => import(/* webpackChunkName: "Login_Home_Welcome" */ '../components/Welcome.vue')
const Userlist = () => import(/* webpackChunkName: "Login_Home_Welcome" */ '../components/useradmin/Userlist.vue')
const Merchantlist = () => import(/* webpackChunkName: "Login_Home_Welcome" */ '../components/useradmin/Merchantlist.vue')
const Productlist = () => import(/* webpackChunkName: "Login_Home_Welcome" */ '../components/productlist/Productlist.vue')
const Customerlist = () => import(/* webpackChunkName: "Login_Home_Welcome" */ '../components/customer/Customerlist.vue')
const Adminuserlist = () => import(/* webpackChunkName: "Login_Home_Welcome" */ '../components/adminuser/Adminuserlist.vue')

Vue.use(VueRouter)

const routes = [
  {
    path:"/",
    redirect:'/login'
  },
 {
   path:"/login",
   component:Login
 },
 { path: '/home',
  component: Home,
  redirect: '/welcome',
  children: [
    { path: '/welcome', component: Welcome },
    { path: '/userlist', component: Userlist },
    { path: '/merchantlist', component: Merchantlist },
    { path: '/productlist', component: Productlist },
    { path: '/customerlist', component: Customerlist },
    { path: '/adminuserlist', component: Adminuserlist }
 
  ] }
]

const router = new VueRouter({
   mode: 'history',
  base: process.env.BASE_URL,
  routes
})
const VueRouterPush = VueRouter.prototype.push

VueRouter.prototype.push = function push (to) {

      return VueRouterPush.call(this, to).catch(err => err)

}

export default router
