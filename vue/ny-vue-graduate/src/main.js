import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import axios from 'axios'
import './assets/css/global.css'
import  'element-ui/lib/theme-chalk/index.css'

axios.defaults.baseURL = 'http://localhost:8889'
//axios.defaults.baseURL = 'http://localhost:8888'
Vue.config.productionTip = false
// 挂在到Vue实例，后面可通过this调用
Vue.prototype.$http = axios
Vue.use(ElementUI)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
