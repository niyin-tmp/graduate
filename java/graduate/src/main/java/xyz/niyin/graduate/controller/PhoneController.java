package xyz.niyin.graduate.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.User;
import xyz.niyin.graduate.mapper.UserMapper;
import xyz.niyin.graduate.utils.JWTUtils;
import xyz.niyin.graduate.utils.Result;

@RequestMapping("/phone")
@Controller
public class PhoneController {
@Autowired
UserMapper userMapper;
@RequestMapping("/getphone")
@ResponseBody
public Result getphone(String token) {
	Result result=new Result();
	//System.out.println(token);
	//校验token
	try {
		DecodedJWT ok= JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		//System.out.println();
		User user=userMapper.selectUser(openid);
		
			String phone=user.getPhone();
			//System.out.println(phone);
			JSONObject phones=new JSONObject();
			if(phone.isEmpty()) {
				//System.out.println("nullll");
				phones.put("phone","");
			}else {
				phones.put("phone",phone);
			}
		return result.sussess(phones);
		//System.out.println(token);
		//System.out.println("校验成功");
		
	} catch (Exception e) {
		// TODO: handle exception
		JSONObject phones=new JSONObject();
		phones.put("phone","");
		return result.sussess("error");
	}
	
}
@RequestMapping("/relievephone")
@ResponseBody
public Result relievephone(String token) {
	Result result=new Result();
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String openid=claims.get("openid").asString();
	userMapper.relievephone(openid);
	return result.sussess("ok");
}
}
