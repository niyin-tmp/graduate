package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class Adminuser {
private int id;
private String adminusername;
private String adminpassword;
private String utype;
private String phone;
private String email;
}
