package xyz.niyin.graduate.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.ProCart;
import xyz.niyin.graduate.entity.Productimgs;
import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.ProCartMapper;
import xyz.niyin.graduate.mapper.ProductimgsMapper;

import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.utils.BosUtil;
import xyz.niyin.graduate.utils.JWTUtils;

@RequestMapping("/cart")
@RestController
public class ProCartController {
	@Autowired
	ProCartMapper proCartMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	@RequestMapping("/getprocarts")
	public JSONObject getprocarts(		
			@RequestParam(value = "token", required = false)String token) {
		JSONObject retunObject = new  JSONObject();
		JSONObject data = new  JSONObject();
		JSONArray datas = new  JSONArray();
		//System.out.println(productId);
		//校验token并获取token
		DecodedJWT ok= JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		//获取购物车信息
		List<ProCart>  procart=proCartMapper.selectcarts(openid);
		for(int i=0;i<procart.size();i++) {
			JSONObject prObject = new  JSONObject();
			JSONArray arra = new  JSONArray();
			JSONArray arrb = new  JSONArray();
			String productId=procart.get(i).getProductId();
			//获取到商品详细信息
			Productlist Productinfo= productlistMapper.selectProductinfo(productId);
			List<Productimgs>  proimgList=productimgsMapper.selectProductimg(productId);
			BosUtil bosUtil=new BosUtil();
			//System.out.println(proimgList);
			for(int j=0;j<proimgList.size();j++) {
				//System.out.println(proimgList.get(i));
				//获取到精选图
				if(proimgList.get(j).getImgtype().equals("0")) {
					arra.add(bosUtil.getimgurl(proimgList.get(j).getImgname()));
				}
				//获取到详情图
				if(proimgList.get(j).getImgtype().equals("1")) {
					arrb.add(bosUtil.getimgurl(proimgList.get(j).getImgname()));
				}
			}
			prObject.put("Productinfo",Productinfo);
			prObject.put("checked",false);
			prObject.put("num",1);
			prObject.put("arra",arra);
			prObject.put("arrb",arrb);
			datas.add(prObject);
			//System.out.println(Productinfo);
		}
		//获取商品信息
		retunObject.put("code", 0);
		retunObject.put("data", datas);
		retunObject.put("message", "");
		//System.out.println(data);
		return retunObject;
	}
	//移除购物车某商品
	@RequestMapping("/deleteproductcart")
	public JSONObject deleteproductcart(
			@RequestParam(value = "token", required = false)String token,
			@RequestParam(value = "productId", required = false)String productId
			) {
		JSONObject retunObject = new  JSONObject();
		//获取商品信息
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		proCartMapper.deleteproductcart(openid, productId);
		retunObject.put("code", 0);
		retunObject.put("data", "ok");
		retunObject.put("message", "");
		return retunObject;	
	     }
}
