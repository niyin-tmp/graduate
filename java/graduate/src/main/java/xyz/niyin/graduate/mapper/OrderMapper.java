package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.Orders;

public interface OrderMapper {
	//openid,ordernum,orderNo,status,productId,num,price,numprice
public List<Orders> selectOrders(@Param("openid")String openid);
public List<Orders> selectOrderlist(@Param("storeid")String storeid);
public void setorderstatus(@Param("openid")String openid,@Param("ordernum")String ordernum,@Param("status")String status);
public void insertOrder(@Param("openid")String openid,@Param("storeid")String storeid,
		@Param("ordernum")String ordernum,@Param("orderNo")String orderNo,@Param("status")String status,
		@Param("productId")String productId,@Param("proname")String proname,@Param("num")int num,
		@Param("price")double price,@Param("numprice")double numprice,@Param("name")String name,
		@Param("phone")String phone,@Param("address")String address);
}
