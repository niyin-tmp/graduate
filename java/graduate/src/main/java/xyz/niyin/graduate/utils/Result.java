package xyz.niyin.graduate.utils;

import lombok.Data;

@Data
public class Result {
private int code;
private String msg;
private Object data;
public Result() {
}

public Result(int code, String msg) {
    this.code = code;
    this.msg = msg;
}

public Result(int code, String msg, Object data) {
    this.code = code;
    this.msg = msg;
    this.data = data;
}

/**
 * 默认的成功
 * @return
 */
public Result success(){
    return new Result(Status.SUCCESS.Code,Status.SUCCESS.msg);
}

/**
 * 默认的失败
 * @return
 */
public Result error(){
    return new Result(Status.FAIL.Code,Status.FAIL.msg);
}

/**
 * 成功 + 返回的成功信息
 * @param data
 * @return
 */
public Result sussess(Object data){
    return new Result(Status.SUCCESS.Code,Status.SUCCESS.msg,data);
}

public enum Status {
    SUCCESS(0,"成功"),
    FAIL(1,"失败");

    private int Code;
    private String msg;

    Status(int code, String msg) {
        Code = code;
        this.msg = msg;
    }
}
}
