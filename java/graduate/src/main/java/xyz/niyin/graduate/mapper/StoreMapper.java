package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import xyz.niyin.graduate.entity.Store;

@Repository
public interface StoreMapper {
	public List<Store> selectStorelist();
	public Store selectStore(@Param("AdminOpenid")String AdminOpenid);
	public Store selectStoreid(@Param("id")String id);
	public void setaddress(@Param("AdminOpenid")String AdminOpenid,@Param("latitude")double latitude,@Param("longitude")double longitude,@Param("address")String address);
	public List<Store> selectnearby(@Param("minlat")double minlat,@Param("maxlat")double maxlat,@Param("minlng")double minlng,@Param("maxlng")double maxlng);
	public void setaddminupdate(@Param("adminOpenid")String adminOpenid,
										@Param("storeName")String storeName,
										@Param("storeType")String storeType,
										@Param("latitude")String latitude,
										@Param("longitude")String longitude,
										@Param("address")String address
										);
}
