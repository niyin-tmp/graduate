package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.Productimgs;

@Mapper
public interface ProductimgsMapper {
	public List<Productimgs> selectProductimg(@Param("productId")String productId);
	public Productimgs selectProductimgone(@Param("productId")String productId);
	public void insertProductimg(
			@Param("productId")String productId,
			@Param("imgname")String imgname,
			@Param("imgtype")String imgtype
			) ;
	public void deleteProductimg(@Param("imgname")String imgname);
	public void deleteProductimgid(@Param("productId")String productId);
	
}
