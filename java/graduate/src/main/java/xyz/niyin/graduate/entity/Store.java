package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class Store {
	private int Id;
	private String storeName;
	private String AdminOpenid;
	private String StoreType;
	private double latitude;
	private double longitude;
	private String address;
	
}
