package xyz.niyin.graduate.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest.H2ConsoleRequestMatcher;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.niyin.graduate.service.UserService;
import xyz.niyin.graduate.utils.Result;


@RequestMapping("/api/mp")
@Controller
public class UserController {
	@Autowired
	UserService userService;
	
	//微信端授权登录
	@GetMapping("/wxlogin")  
	@ResponseBody
	//可以通过required=false或者true来要求@RequestParam配置的前端参数是否一定要传  
	public Result user_wxlogin(@RequestParam(value = "code", required = false) String code,
							   @RequestParam(value = "rawData", required = false) String rawData,
							   @RequestParam(value = "signature", required = false) String signature,
							   @RequestParam(value = "encrypteData", required = false) String encrypteData,
							   @RequestParam(value = "iv", required = false) String iv) {
		//System.out.println(rawData);
		Result result=userService.Wxlogin(code, rawData);
	    return result;
	}
}
