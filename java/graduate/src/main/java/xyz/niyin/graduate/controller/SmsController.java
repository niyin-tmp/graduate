package xyz.niyin.graduate.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.security.auth.kerberos.KerberosKey;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;

import xyz.niyin.graduate.entity.SmsCode;
import xyz.niyin.graduate.mapper.SmsCodeMapper;
import xyz.niyin.graduate.mapper.UserMapper;
import xyz.niyin.graduate.utils.JWTUtils;
import xyz.niyin.graduate.utils.Result;
import xyz.niyin.graduate.utils.SmsUtil;


@RequestMapping("/sms")
@Controller
public class SmsController {

	@Autowired
	UserMapper userMapper;
	@Autowired
	SmsCodeMapper smsCodeMapper;
@RequestMapping("/sendcode")
@ResponseBody
public JSONObject sendcode(
		@RequestParam(value = "token", required = false) String token,
		@RequestParam(value = "phone", required = false) String phone,
		@RequestParam(value = "type", required = false) String type
		) {
	JSONObject retunObject=new JSONObject();
	SmsUtil smsUtil = new SmsUtil();
	long settime=DateTime.now().getMillis() / 1000;
	System.out.println(settime);
	
	String code=(int)((Math.random()*9+1)*1000)+"";
	System.out.println(code);
	smsCodeMapper.deletesmscode(phone, type);
	smsCodeMapper.addsmscode(token,phone, code, type, settime);
	smsUtil.sendcode(phone,code);
	retunObject.put("code", 0);
	retunObject.put("data", "ok");
	retunObject.put("message", "");
	return retunObject;
}
@RequestMapping("/verifycode")
@ResponseBody
public JSONObject bindverifycode(
		@RequestParam(value = "token", required = false) String token,
		@RequestParam(value = "phone", required = false) String phone,
		@RequestParam(value = "code", required = false) String code,
		@RequestParam(value = "type", required = false) String type) {
	//认证token再进行操作
	DecodedJWT ok = JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String openid=claims.get("openid").asString();
	//认证手机
	//认证类型
	JSONObject data = new JSONObject();
	Result result=new Result();
	SmsUtil smsUtil=new SmsUtil();
	//type=0绑定手机号
	long newtime=DateTime.now().getMillis() / 1000;
	System.out.println("newtime:"+newtime);
	List<SmsCode> arr=smsCodeMapper.selectsmscode(phone, type);
	System.out.println(smsCodeMapper.selectsmscode(phone, type));
	if(arr.size()!=0) {
	for(int i=0;i<arr.size();i++) {
		long ss=arr.get(i).getSettime();
		//小于10分钟有效
		if(newtime-ss<60*10) {
			if(arr.get(i).getCode().equals(code)) {
				
				//做绑定操作
				userMapper.updatephone(openid, phone);
				data.put("code", 0);
				data.put("data", "ok");
				data.put("message", "");
				//返回正确信息
				//清楚验证码
			}else {
				data.put("code", 0);
				data.put("data", "验证码无效");
				data.put("message", "");
				//返回错误信息
			}
			//验证通过并清除验证码
		}else {
			//清除验证码
			data.put("code", 0);
			data.put("data", "验证码无效");
			data.put("message", "");
			//返回错误信息
		}
	  }
	}else {
		data.put("code", 0);
		data.put("data", "验证码无效");
		data.put("message", "");
	}
	return data;
}
}
