package xyz.niyin.graduate.service;

import java.io.IOException;
import java.net.URL;

import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {
//把文件上传到百度云
	public String mk(MultipartFile uploadFile,String imgname) throws IOException;
	public String deleteimg(String filename);
}
