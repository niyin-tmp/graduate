package xyz.niyin.graduate.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import xyz.niyin.graduate.service.UploadFileService;


@RequestMapping("/submit")
@RestController
public class ReleaseController {
	@Autowired
	UploadFileService uploadFileService;
	//本控制器用于商家产品的发布
	@RequestMapping("v1")
	public JSONObject hello(
			@RequestParam(value = "content", required = false) MultipartFile content,
			@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "product_id", required = false) String product_id,
			@RequestParam(value = "product_name", required = false) String product_name,
			@RequestParam(value = "product_number", required = false) String product_number,
			@RequestParam(value = "product_peice", required = false) String product_peice,
			@RequestParam(value = "dproduct_describe", required = false) String dproduct_describe
			) {
		
		System.out.println("----");
		System.out.println(content.getContentType());
		System.out.println("token=" + token);
		System.out.println("product_id=" + product_id);
		System.out.println("product_name=" + product_name);
		System.out.println("product_number" + product_number);
		System.out.println("product_peice=" + product_peice);
		System.out.println("dproduct_describe=" + dproduct_describe);
		
		JSONObject data = new JSONObject();
		JSONObject jsobj1 = new JSONObject();
		data.put("data", "验证通过");
		jsobj1.put("code", 0);
		jsobj1.put("data", data);
		jsobj1.put("message", "");
		return jsobj1;
	}
}
