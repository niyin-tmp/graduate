package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.History;


public interface HistoryMapper {
	public void insertHistory(@Param("openid")String openid,@Param("productId")String productId);
	public List<History> selectHistorypro(@Param("openid")String openid);
	public History selectHistory(@Param("openid")String openid,@Param("productId")String productId); 
	public void deleteHistory(@Param("openid")String openid,@Param("productId")String productId);
}
