package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.ChatHistory;


public interface ChatHistoryMapper {

public void insertchat(@Param("useropenid")String useropenid,@Param("storeopenid")String storeopenid,
		@Param("content")String content,@Param("status")String status,@Param("userstatus")String userstatus,@Param("contype")String contype);
public List<ChatHistory> selectchat(@Param("useropenid")String useropenid, @Param("storeopenid")String storeopenid);

public List<ChatHistory> selectstatus(@Param("useropenid")String useropenid,@Param("storeopenid")String storeopenid);
public List<ChatHistory> selectuserstatus(@Param("useropenid")String useropenid,@Param("storeopenid")String storeopenid);
public List<ChatHistory> selectuser(@Param("storeopenid")String storeopenid);
public List<ChatHistory> selectstore(@Param("useropenid")String useropenid);
public void updatestatus(@Param("useropenid")String useropenid,@Param("storeopenid")String storeopenid);

public void updateuserstatus(@Param("useropenid")String useropenid,@Param("storeopenid")String storeopenid);
}
