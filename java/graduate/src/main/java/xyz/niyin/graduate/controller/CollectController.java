package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.Collect;
import xyz.niyin.graduate.entity.Productimgs;
import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.CollectMapper;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.utils.BosUtil;
import xyz.niyin.graduate.utils.JWTUtils;



@RequestMapping("/collect")
@RestController
public class CollectController {
	@Autowired
	CollectMapper collectMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
//获取购物车商品信息
@RequestMapping("/getcollect")
public JSONObject getcollect(String token) {
	JSONObject data = new JSONObject();
	collectMapper.selectcollects("xxx");
	//查询商品ID
	DecodedJWT verify= JWTUtils.getTokenInfo(token);
	String AdminOpenid=verify.getClaim("openid").asString();
	//System.out.println(collectMapper.selectcollects("oeqhO5W48DpAtUdwJjxD4GPJdmvQ"));
	List<Collect> arr=collectMapper.selectcollects(AdminOpenid);
	JSONArray result1=new JSONArray();
	//遍历购物车商品ID，通过id获取商品信息
	for(int i=0;i<arr.size();i++) {
		JSONObject jsonObjectData = new JSONObject();
		String productId=arr.get(i).getProductId();
		//System.out.println(productlistMapper.selectProductinfo(arr.get(i).getProductId()));
		Productlist product=productlistMapper.selectProductinfo(arr.get(i).getProductId());
		//放图片
		List<Productimgs> lists=productimgsMapper.selectProductimg(productId);
		jsonObjectData.put("product",product);
		
		JSONArray jsonArray1=new JSONArray();
		JSONArray jsonArray2=new JSONArray();
		BosUtil bosUtil=new BosUtil();
		for(int j=0;j<lists.size();j++) {
			//JSONObject jsonObjectDatas = new JSONObject();
			//System.out.println();
			//System.out.println(lists.get(j));
			//获取文件类型
			String tyString=lists.get(j).getImgtype();
			//精选照片
			if(tyString.equals("0")) {
				jsonArray1.add(bosUtil.getimgurl(lists.get(j).getImgname()));
			}
			//详情照片
			if(tyString.equals("1")) {
				jsonArray2.add(bosUtil.getimgurl(lists.get(j).getImgname()));
			}
			//System.out.println("文件路径==="+bosUtil.getimgurl(lists.get(j).getImgname()));
		}
		jsonObjectData.put("productImgurlSift",jsonArray1);
		jsonObjectData.put("productImgurlDetails",jsonArray2);
		jsonObjectData.put("checked",false);
		result1.add(jsonObjectData);
	}
	
	
	data.put("code", 0);
	data.put("data", result1);
	data.put("message", "");
	
	return data;
	
}
//删除购物车记录
@RequestMapping("/deletecollect")
public JSONObject deletecollect(String token,String productId) {
	JSONObject data = new JSONObject();
	System.out.println(productId);
	DecodedJWT verify=JWTUtils.getTokenInfo(token);
	String openid=verify.getClaim("openid").asString();
	collectMapper.deletecollect(openid, productId);
	data.put("code", 0);
	data.put("data", "ok");
	data.put("message", "");
	return data;
}



}
