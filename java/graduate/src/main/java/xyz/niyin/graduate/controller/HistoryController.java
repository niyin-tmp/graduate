package xyz.niyin.graduate.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.History;
import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.HistoryMapper;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.utils.BosUtil;
import xyz.niyin.graduate.utils.JWTUtils;

@RestController
@RequestMapping("/history")
public class HistoryController {
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	HistoryMapper historyMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	@RequestMapping("/gethistorypro")
	public JSONObject gethistorypro(String token) {
		JSONObject obj=new JSONObject();
		System.out.println(token);
		DecodedJWT ok = JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		List<History> history=historyMapper.selectHistorypro(openid);
		System.out.println(historyMapper.selectHistorypro(openid));
		BosUtil bosUtil=new BosUtil();
		//obj.put("history",history);
		JSONArray hisArray=new JSONArray();
		for(int i=0;i<history.size();i++) {
			//System.out.println(history.get(i).getProductId());
			String productId=history.get(i).getProductId();
			Productlist historypro=productlistMapper.selectProductinfo(productId);
			//productimgsMapper.selectProductimgone(productId);
			//System.out.println(productimgsMapper.selectProductimgone(productId).getImgname());
			//logo
			String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
			System.out.println(bosUtil.getimgurl(imgname));
			String logo=bosUtil.getimgurl(imgname);
			//System.out.println(productlistMapper.selectProductinfo(productId));
			JSONObject arr=new JSONObject();
			arr.put("historypro",historypro);
			arr.put("productId",productId);
			arr.put("logo",logo);
			hisArray.add(arr);
		}
		obj.put("hisArray",hisArray);
		JSONObject data=new JSONObject();
		data.put("code",0);
		data.put("data",obj);
		data.put("message","");
		return data;
	}
	@RequestMapping("/delhistory")
	public JSONObject delhistory(String token,String productId) {
		JSONObject data=new JSONObject();
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		historyMapper.deleteHistory(openid, productId);
		data.put("code",0);
		data.put("data","ok");
		data.put("message","");
		return data;
	}

}
