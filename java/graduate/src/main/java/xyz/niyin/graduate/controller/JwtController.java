package xyz.niyin.graduate.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import xyz.niyin.graduate.utils.JWTUtils;


@RequestMapping("/jwt")
@RestController
public class JwtController {
	@RequestMapping("/jwtveriitfy")
	public JSONObject jwtveriitfy(String token) {
		JSONObject jsobj = new JSONObject();
		//System.out.println(token);
		
		try {
			JWTUtils.verify(token);
			jsobj.put("code", 0);
			jsobj.put("data", "login");
			jsobj.put("message", "");
		} catch (Exception e) {
			jsobj.put("code", 0);
			jsobj.put("data", "logout");
			jsobj.put("message", "");
			// TODO: handle exception
		}
		
		

		return jsobj;
	}
}
