package xyz.niyin.graduate.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.ansj.dic.impl.Url2Stream;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.builder.StandardToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.Productimgs;
import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.mapper.StoreMapper;
import xyz.niyin.graduate.service.UploadFileService;
import xyz.niyin.graduate.utils.BosUtil;
import xyz.niyin.graduate.utils.JWTUtils;

@RequestMapping("/product")
@RestController
public class ProductController {
	@Autowired
	StoreMapper storeMapper;
	@Autowired
	UploadFileService uploadFileService;
	@Autowired
	ProductimgsMapper productimgsMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	//用于产品发布上传图片
	@RequestMapping("v1")
	public JSONObject uploadimg(
			@RequestParam(value = "content", required = false) MultipartFile content,
			@RequestParam(value = "product_id", required = false) String productId,
			@RequestParam(value = "symbol", required = false) String symbol,
			@RequestParam(value = "imgtype", required = false) String imgtype,
			@RequestParam(value = "token", required = false) String token
			) {
		//System.out.println(content);
		System.out.println("id="+productId);
		//System.out.println("type="+imgtype);
		//System.out.println("token="+token);
		//校验并解析token,拿到用户信息
		System.out.println("symbol="+symbol);
		DecodedJWT ok= JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		String id=claims.get("id").asString();
		System.out.println("id="+id);
		//System.out.println("openid="+openid);
        // 获取数据流
        String originalFilename  = content.getOriginalFilename();
        System.out.println(originalFilename);
      //获取最后一个.的位置
        int lastIndexOf = originalFilename.lastIndexOf(".");
        //获取文件的后缀名 .jpg
        String suffix = originalFilename.substring(lastIndexOf);

        //System.out.println("suffix = " + suffix);
        
        //文件名拼接
		String imgname=productId+"-"+imgtype+"-"+symbol+"-"+openid+suffix;
		
		//上传图片到百度云存储
		String productImgurl="";
		try {
			productImgurl=uploadFileService.mk(content,imgname);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		productimgsMapper.insertProductimg(productId,imgname, imgtype);
		
		//把商品id,图片类型，url存到数据库
		
		
		
		/*
		 * Productlist list=productlistMapper.selectProduct(productId); if(list!=null) {
		 * System.out.println("商品已存在,正在插入精选图片");
		 * 
		 * }else { productlistMapper.insertProduct(productId, productName, productNum,
		 * productPrice, productExplain); System.out.println("插入商品信息"); }
		 */
		
		//查询商品精选图片
		//System.out.println(productimgsMapper.selectProductimg(productId));

		JSONObject data = new JSONObject();
		
		JSONObject jsobj = new JSONObject();
		/*
		 * String[] arr=new String[1]; arr[0]=productImgurl; data.put("imgurl", arr);
		 */
		jsobj.put("code", 0);
		jsobj.put("data", data);
		jsobj.put("message", "");
		return jsobj;
	}
	//用于产品发布上传基本资料
	@RequestMapping("v2")
	public JSONObject uploadimg(
			@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "product_id", required = false) String productId,
			@RequestParam(value = "product_name", required = false) String productName,
			@RequestParam(value = "product_number", required = false) String productNum,
			@RequestParam(value = "product_peice", required = false) String productPrice,
			@RequestParam(value = "product_type", required = false) String productType,
			@RequestParam(value = "dproduct_describe", required = false) String productExplain) {
		    System.out.println("类型"+productType);
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String AdminOpenid=claims.get("openid").asString();
		String id=claims.get("id").asString();
		//得到用户id
		System.out.println("id="+id);
		//产生6位随机数
		String string=(int)((Math.random()*9+1)*100000)+"";
		System.out.println("string="+string);
		//拼接商品id,避免重复
		productId=productId+id+string;
		System.out.println("上传信息");
		String storeid= storeMapper.selectStore(AdminOpenid).getId()+"";
		//把商品信息上传到数据库
		productlistMapper.insertProduct(productId,storeid ,productName, productNum,
				 productPrice, productType,productExplain);
		JSONObject jsobj = new JSONObject();
		jsobj.put("code", 0);
		jsobj.put("data", productId);
		jsobj.put("message", "");
		return jsobj;	
	}
	//用于产品管理
	@RequestMapping("v3")
	//产品管理回调
	public JSONObject manage(@RequestParam(value = "token", required = false) String token) {
		JSONObject jsobj = new JSONObject();
		JSONObject data = new JSONObject();
		//拿到商家openid
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String AdminOpenid=claims.get("openid").asString();
		//拿到店铺id
		int storeid= storeMapper.selectStore(AdminOpenid).getId();
		
		//productlistMapper.selectProduct(storeid);
		//System.out.println(productlistMapper.selectProduct(storeid));
		List<Productlist> list=productlistMapper.selectProduct(storeid);
		
		JSONArray jsonArray=new JSONArray();
		for(int i=0;i<list.size();i++) {
			 JSONObject jsonObjectData = new JSONObject();
			//System.out.println(list.get(i));
			jsonObjectData.put("ProductId",list.get(i).getProductId());
			jsonObjectData.put("ProductName",list.get(i).getProductName());
			jsonObjectData.put("ProductNum",list.get(i).getProductNum());
			jsonObjectData.put("ProductPrice",list.get(i).getProductPrice());
			jsonObjectData.put("ProductType",list.get(i).getProductType());
			jsonObjectData.put("ProductExplain",list.get(i).getProductExplain());
			//放图片
			List<Productimgs> lists=productimgsMapper.selectProductimg(list.get(i).getProductId());
			JSONArray jsonArray1=new JSONArray();
			JSONArray jsonArray2=new JSONArray();
			BosUtil bosUtil=new BosUtil();
			for(int j=0;j<lists.size();j++) {
				//JSONObject jsonObjectDatas = new JSONObject();
				//System.out.println();
				//System.out.println(lists.get(j));
				//获取文件类型
				String tyString=lists.get(j).getImgtype();
				//精选照片
				if(tyString.equals("0")) {
					jsonArray1.add(bosUtil.getimgurl(lists.get(j).getImgname()));
				}
				//详情照片
				if(tyString.equals("1")) {
					jsonArray2.add(bosUtil.getimgurl(lists.get(j).getImgname()));
				}

							
				//System.out.println("文件路径==="+bosUtil.getimgurl(lists.get(j).getImgname()));
				
				
			}
			//jsonObjectData.put("ProductPrice",list.get(i).getProductPrice());
			//精选图片
			jsonObjectData.put("productImgurlSift",jsonArray1);
			 //详情图片
			jsonObjectData.put("productImgurlDetails",jsonArray2);
			//System.out.println(lists);
			jsonArray.add(jsonObjectData);
			data.put("Productlist",jsonArray);
		}
		
		
		
		jsobj.put("code", 0);
		jsobj.put("data", data);
		jsobj.put("message", "");
		return jsobj;
	} 
	//用于产品信息修改
	@RequestMapping("v4")
	public JSONObject updateProduct(@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "product_id", required = false) String productId,
			@RequestParam(value = "product_name", required = false) String productName,
			@RequestParam(value = "product_number", required = false) String productNum,
			@RequestParam(value = "product_peice", required = false) String productPrice,
			@RequestParam(value = "product_type", required = false) String productType,
			@RequestParam(value = "dproduct_describe", required = false) String productExplain) {
		productlistMapper.updateProduct(productId,productName, productNum, productPrice, productType,productExplain);
		
		JSONObject jsobj = new JSONObject();
		System.out.println("更改信息");
		jsobj.put("code", 0);
		jsobj.put("data", "ok");
		jsobj.put("message", "");
		return jsobj;
		
		
	}
	//用于修改商品图片
	@RequestMapping("v5")
	public JSONObject updateProImg(
			@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "oldarra", required = false) String oldarra,
			@RequestParam(value = "oldarrb", required = false) String oldarrb,
			@RequestParam(value = "arra", required = false) String arra,
			@RequestParam(value = "arrb", required = false) String arrb
			) {
		System.out.println(token);
		System.out.println("----");
		System.out.println(oldarrb);
		System.out.println("+++");
		System.out.println(arrb);
		//JSONObject json = JSON.parseObject(oldarra);
		JSONArray arraya=(JSONArray) JSONArray.parse(oldarra);
		JSONArray arrayb=(JSONArray) JSONArray.parse(oldarrb);
		System.out.println(arrayb.size());
		System.out.println(arraya.size());
		//增加精选图片
		
		//精选图片删除
		  for(int i=0;i<arraya.size();i++) { 
			  String string=(String) arraya.get(i);
			  String str1 = string.substring(0, 39);
			  System.out.println(str1);
			  if(str1.equals("https://bj.bcebos.com/v1/photo-reports/")) {
				  //获取得到文件名
				  String imgname = string.substring(39, string.indexOf("?"));
				  System.out.println(imgname);
				  //删除百度云存储的文件
				  uploadFileService.deleteimg(imgname);
				  productimgsMapper.deleteProductimg(imgname);
				  //获取得到文件类型
				 // System.out.println(filename.substring(filename.indexOf("-")+1,filename.indexOf("-")+2));
			  }else {
				  System.out.println("操作失败");
			  }
		  }
		    //增加详情图片
		  
			//详情图片删除
		  for(int i=0;i<arrayb.size();i++) { 
			  String string=(String) arrayb.get(i);
			  String str1 = string.substring(0, 39);
			  System.out.println(str1);
			  if(str1.equals("https://bj.bcebos.com/v1/photo-reports/")) {
				  //获取得到文件名
				  String imgname = string.substring(39, string.indexOf("?"));
				  System.out.println(imgname);
				  //删除百度云存储的文件
				  uploadFileService.deleteimg(imgname);
				  productimgsMapper.deleteProductimg(imgname);
				  //获取得到文件类型
				 // System.out.println(filename.substring(filename.indexOf("-")+1,filename.indexOf("-")+2));
			  }else {
				  System.out.println("操作失败");
			  }
		  }
		  
		  
		JSONObject jsobj = new JSONObject();
		System.out.println("更改图片信息");
		jsobj.put("code", 0);
		jsobj.put("data", "ok");
		jsobj.put("message", "");
		return jsobj;
	}
	//删除商品信息
	@RequestMapping("v6")
	public JSONObject deleteProduct(@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "product_id", required = false) String productId) {
		System.out.println("删除商品"+productId);
		List<Productimgs> lists =	productimgsMapper.selectProductimg(productId);
		for(int i=0;i<lists.size();i++) {
			System.out.println(lists.get(i).getImgname());
			//删除云存储的商品图片
			String imgname=lists.get(i).getImgname();
			uploadFileService.deleteimg(imgname);
		}
		//删除商品基本信息
		productlistMapper.deleteProduct(productId);
		//删除商品图片信息
		productimgsMapper.deleteProductimgid(productId);
		JSONObject jsobj = new JSONObject();
		System.out.println("更改图片信息");
		jsobj.put("code", 0);
		jsobj.put("data", "删除ok");
		jsobj.put("message", "");
		return jsobj;
		
	}
	//修改增加图片
	@RequestMapping("v7")
	public JSONObject updateimg(
			@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "content", required = false) MultipartFile content,
			@RequestParam(value = "imgtype", required = false) String imgtype,
			@RequestParam(value = "product_id", required = false) String productId) {
		
		System.out.println("============================");
		//System.out.println(productId);
		//System.out.println(token);
		//System.out.println(content);
		//System.out.println("类型"+imgtype);
	
		//获取openid
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		//System.out.println(openid+"--openid");
		//查询文件名保证不重复

		
		//System.out.println(arr1.get(0));
		//System.out.println(arr2.get(0));
		//获取文件后缀名
        String originalFilename  = content.getOriginalFilename();
        //System.out.println(originalFilename);
      //获取最后一个.的位置
        int lastIndexOf = originalFilename.lastIndexOf(".");
        //获取文件的后缀名 .jpg
        String suffix = originalFilename.substring(lastIndexOf);
		//拼接文件名

		JSONObject jsobj = new JSONObject();
		
		//System.out.println("更改图片信息");
		jsobj.put("code", 0);
		jsobj.put("data", "修改ok");
		jsobj.put("message", "");
		return jsobj;
	}
	//删除商品信息
	@RequestMapping("v8")
	public JSONObject getimgid(@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "product_id", required = false) String productId) {
		
		System.out.println("00000");
		System.out.println(token);
		System.out.println(productId);
		
		List<Productimgs> lists =	productimgsMapper.selectProductimg(productId);
		List arr1=new ArrayList();
		arr1.add("0");
		arr1.add("1");
		arr1.add("2");
		arr1.add("3");
		arr1.add("4");
		List arr2=new ArrayList();
		arr2.add("0");
		arr2.add("1");
		arr2.add("2");
		arr2.add("3");
		arr2.add("4");
		for(int i=0;i<lists.size();i++) {
			String filename=lists.get(i).getImgname();
			String ty=lists.get(i).getImgtype();
			if(ty.equals("0")) {
				arr1.remove(filename.substring(23, 24));
				
			}if(ty.equals("1")) {
				arr2.remove(filename.substring(23, 24));
			}
			System.out.println("原有=="+filename);
		}
		System.out.println(arr1);
		System.out.println(arr2);
		
		JSONObject jsobj = new JSONObject();
		JSONObject jsobj2 = new JSONObject();
		jsobj2.put("arr1",arr1);
		jsobj2.put("arr2",arr2);
		//System.out.println("更改图片信息");
		jsobj.put("code", 0);
		jsobj.put("data", jsobj2);
		jsobj.put("message", "");
		return jsobj;
		
	}
	
}
