package xyz.niyin.graduate.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import xyz.niyin.graduate.entity.UserAddress;
import xyz.niyin.graduate.mapper.UserAddressMapper;
import xyz.niyin.graduate.utils.JWTUtils;


@RequestMapping("/useraddress")
@RestController
public class UserAddressController {
@Autowired
UserAddressMapper userAddressMapper;
//添加收货地址
@RequestMapping("/setaddress")	
public JSONObject setaddress(String token,String name,String phone,String address) {
	JSONObject retunObject=new JSONObject();
	DecodedJWT ok= JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String openid=claims.get("openid").asString();
	String status="0";
	userAddressMapper.setuseraddress(openid, status, name, phone, address);
	System.out.println("sss");
	retunObject.put("code", 0);
	retunObject.put("data", "ok");
	retunObject.put("message", "");
	//System.out.println(data);
	return retunObject;
}
//获取收货地址
@RequestMapping("/getaddress")	
public JSONObject getaddress(String token) {
	JSONObject retunObject=new JSONObject();
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String openid=claims.get("openid").asString();
	List<UserAddress> arr=userAddressMapper.selectaddress(openid);
	System.out.println(arr);
	retunObject.put("code", 0);
	retunObject.put("data", arr);
	retunObject.put("message", "");
	//System.out.println(data);
	return retunObject;
}
//删除地址
@RequestMapping("/deleteaddress")	
public JSONObject delete(String token,int id) {
	JSONObject retunObject=new JSONObject();
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String openid=claims.get("openid").asString();
	userAddressMapper.deleteaddress(openid,id);
	retunObject.put("code", 0);
	retunObject.put("data", "ok");
	retunObject.put("message", "");
	//System.out.println(data);
	return retunObject;
}
@RequestMapping("/setmoaddress")	
public JSONObject setmoaddress(String token,int id) {
	JSONObject retunObject=new JSONObject();
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String openid=claims.get("openid").asString();
	userAddressMapper.updateaddressa(openid);
	userAddressMapper.updateaddressb(openid, id);
	retunObject.put("code", 0);
	retunObject.put("data", "ok");
	retunObject.put("message", "");
	//System.out.println(data);
	return retunObject;
}

}
