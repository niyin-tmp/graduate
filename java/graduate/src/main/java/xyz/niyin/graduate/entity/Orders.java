package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class Orders {
private int id;
//openid
private String openid;
//订单号
private String ordernum;
//支付单号
private String orderNo;
//订单状态
private String status;
//商品id
private String productId;
//商品名称
private String proname;
//订单商品数
private int num;
//商品单价
private double price;
//商品总价
private double numprice;
//姓名
private String name;
//手机
private String phone;
//地址
private String address;
}
