package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.ProCart;


public interface ProCartMapper {
public void addprocart(@Param("openid")String openid,@Param("productId")String productId);
public void deleteproductcart(@Param("openid")String openid,@Param("productId")String productId);
public ProCart selectcart(@Param("openid")String openid, @Param("productId")String productId);
public List<ProCart> selectcarts(@Param("openid")String openid);
}
