package xyz.niyin.graduate.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.*;
import xyz.niyin.graduate.mapper.*;
import xyz.niyin.graduate.utils.JWTUtils;

@RequestMapping("/pcmerchant")
@RestController
public class PCmerchantController {
	@Autowired
	AdminchatMapper adminchatMapper;
	@Autowired
	AdminuserMapper adminuserMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	UserMapper userMapper;
	@Autowired
	StoreMapper storeMapper;
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/login")
	public JSONObject login(@RequestBody Map<String, Object> usermap) {
		
		
		//System.out.println(map.get("username"));
		//System.out.println(map.get("password"));
		String username=(String) usermap.get("username");
		String password=(String) usermap.get("password");
		System.out.println(username);
		System.out.println(password);
		Adminuser user=adminuserMapper.selectAdminlogin(username,password);
		System.out.println(user);
		//System.out.println(user.getUtype());
		//设置token
		if(user!=null) {
			Map<String , String> map = new HashedMap();
			map.put("username", user.getAdminusername());
			map.put("phone", user.getPhone());
			map.put("email", user.getEmail());
			String token= JWTUtils.getToken(map);
			JSONObject returnObject=new JSONObject();
			JSONObject data=new JSONObject();
			returnObject.put("meta",200);
			data.put("token",token);
			data.put("usertype",user.getUtype());
			returnObject.put("data",data);
			return returnObject;
		}else {
			JSONObject returnObject=new JSONObject();
			JSONObject data=new JSONObject();
			returnObject.put("meta",401);
			data.put("data", "");
			data.put("error", "账号密码错误");
			returnObject.put("data",data);
			return returnObject;
		}
		
		
//		Map<String, Claim> claims=JWTUtils.getTokenInfo(token).getClaims();
//		String usernameto=claims.get("username").asString();
//		System.out.println(usernameto);
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/menus")
	public JSONObject menus() {
		JSONObject data=new JSONObject();
		JSONArray arraya=new JSONArray();
		JSONArray array=new JSONArray();
		JSONObject dataobj=new JSONObject();
		JSONObject dataobj1=new JSONObject();
		dataobj1.put("id", 1);
		dataobj1.put("authName", "用户列表");
		array.add(dataobj1);
		dataobj.put("authName", "用户管理");
		dataobj.put("id", 1);
		dataobj.put("order", 1);
		dataobj.put("path", "users");
		dataobj.put("array", array);
		arraya.add(dataobj);
		data.put("meta",200);
		data.put("data",arraya);
		return data;
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/getuserlist")
	public JSONObject getuserlist() {
		JSONObject data=new JSONObject();
		JSONArray userlist=new JSONArray();
		List<User> users= userMapper.selectUserlist();
		
		data.put("meta",200);
		data.put("data",users);
		System.out.println();
		return data;
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/getstorelist")
	public JSONObject getstorelist() {
		JSONObject data=new JSONObject();
		JSONArray userlist=new JSONArray();
		//userMapper.selectUserlist();
		List<Store> storelist= storeMapper.selectStorelist();
		data.put("meta",200);
		data.put("data",storelist);
		System.out.println();
		return data;
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/getprolist")
	public JSONObject getprolist() {
		JSONObject data=new JSONObject();
		JSONArray userlist=new JSONArray();
		//userMapper.selectUserlist();
		List<Productlist> proList=productlistMapper.selectProducts();
		data.put("meta",200);
		data.put("data",proList);
		System.out.println();
		return data;
	}
	
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/getadminuserlist")
	public JSONObject getadminuserlist() {
		JSONObject data=new JSONObject();
		JSONArray userlist=new JSONArray();
		//userMapper.selectUserlist();
		List<Adminuser> adminuserlist= adminuserMapper.selectAdminuserlist();
		data.put("meta",200);
		data.put("data",adminuserlist);
		System.out.println();
		return data;
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/getadminchatlist")
	public JSONObject getadminchatlist(String token) {
		JSONObject data=new JSONObject();
			DecodedJWT ok=JWTUtils.getTokenInfo(token);
			Map<String, Claim> claims=ok.getClaims();
			String useropenid=claims.get("openid").asString();
			List<Adminchat> chatlist=adminchatMapper.selectuserchatlist(useropenid);
			

			data.put("code", 0);
			data.put("data",chatlist);
			data.put("message", "");
		return data;
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/setadminchatlist")
	public JSONObject setadminchatlist(String token,String content,String contype) {
		JSONObject data=new JSONObject();
			DecodedJWT ok=JWTUtils.getTokenInfo(token);
			Map<String, Claim> claims=ok.getClaims();
			String useropenid=claims.get("openid").asString();
			
			String username=claims.get("nickName").asString();
			System.out.println(content);
			System.out.println(contype);
			String status="0";
			String userstatus="0";
//			List<Adminchat> chatlist=adminchatMapper.selectuserchatlist(useropenid);
			adminchatMapper.insertchat(useropenid, username, content, contype, status, userstatus);
			data.put("code", 0);
			data.put("data","ok");
			data.put("message", "");
		return data;
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/getuserchatlist")
	public JSONObject getuserchatlist() {
		JSONObject data=new JSONObject();
		System.out.println(adminchatMapper.selectuser());
		List<Adminchat> userList=adminchatMapper.selectuser();	
			data.put("code", 0);
			data.put("data",userList);
			data.put("message", "");
		return data;
	}
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/getadminchatlista")
	public JSONObject getadminchatlista(String useropenid) {
		System.out.println(useropenid);
		JSONObject data=new JSONObject();
		List<Adminchat> chatlist=adminchatMapper.selectuserchatlist(useropenid);
			
			data.put("code", 0);
			data.put("data",chatlist);
			data.put("message", "");
		return data;
	}
	//setuserchatlist
	@CrossOrigin(origins = "*",maxAge = 3600)
	@RequestMapping("/setuserchatlist")
	public JSONObject setuserchatlist(String useropenid,String username,String content) {
		System.out.println(useropenid);
		System.out.println(content);
		JSONObject data=new JSONObject();
		//List<Adminchat> chatlist=adminchatMapper.selectuserchatlist(useropenid);	
		    String status="0";
		    String userstatus="0";
		    String contype="1";
//		List<Adminchat> chatlist=adminchatMapper.selectuserchatlist(useropenid);
		    adminchatMapper.insertchat(useropenid, username, content, contype, status, userstatus);
			data.put("code", 0);
			data.put("data","ok");
			data.put("message", "");
		return data;
	}
}
