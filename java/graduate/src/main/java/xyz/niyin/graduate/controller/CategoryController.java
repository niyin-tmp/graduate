package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.utils.BosUtil;

@RestController
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	//获取分类数据
	@RequestMapping("/getCategoryPro")
	public JSONObject getCategoryPro() {
		JSONObject data=new JSONObject();
		productlistMapper.selectProducts();
		System.out.println(productlistMapper.selectProducts());
		List<Productlist> arrpro=productlistMapper.selectProducts();
		BosUtil bosUtil=new BosUtil();
		JSONArray array=new JSONArray();
		System.out.println(productlistMapper.selectprotype());
		List<String> productType=productlistMapper.selectprotype();
		JSONObject objs=new JSONObject();
		for (int i = 0; i < arrpro.size(); i++) {
			JSONObject obj=new JSONObject();
			String productId=arrpro.get(i).getProductId();
			String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
			System.out.println(bosUtil.getimgurl(imgname));
			String logo=bosUtil.getimgurl(imgname);
			
			obj.put("proinfo",arrpro.get(i));
			obj.put("logo",logo);
			array.add(obj);
			//System.out.println(arrpro.get(i));
		}
		objs.put("array",array);
		objs.put("productType",productType);
		data.put("code",0);
		data.put("data",objs);
		data.put("message","");
		return data;
	}
}
