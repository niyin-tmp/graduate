package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.Productlist;


public interface ProductlistMapper {
	public List<Productlist> selectProducts();
	public List<Productlist> selectProductsearch(@Param("productName")String productName);
	
	public List<Productlist> selectProductsearchtype(@Param("productType")String productType,@Param("productName")String productName);
	public List<Productlist> selectProductsearchdis(@Param("storeid")int storeid,@Param("productName")String productName);
	public List<Productlist> selectProductsearchdistype(@Param("productType")String productType,@Param("storeid")String storeid,@Param("productName")String productName);
	public Productlist selectProductinfo(@Param("productId")String productId);
	public List<Productlist> selectProduct(@Param("storeid")int storeid);
	public void insertProduct(@Param("productId")String productId,
			@Param("storeid")String storeid,
			@Param("productName")String productName,
			@Param("productNum")String productNum,
			@Param("productPrice")String productPrice,
			@Param("productType")String productType,
			@Param("productExplain")String productExplain
			);
	public void updateProduct(
			@Param("productId")String productId,
			@Param("productName")String productName,
			@Param("productNum")String productNum,
			@Param("productPrice")String productPrice,
			@Param("productType")String productType,
			@Param("productExplain")String productExplain
			);
	public void updateadminProduct(
			@Param("productId")String productId,
			@Param("productName")String productName,
			@Param("productNum")String productNum,
			@Param("productPrice")String productPrice,
			@Param("productType")String productType,
			@Param("pageviews")int pageviews,
			@Param("productExplain")String productExplain
			);
	public void deleteProduct(@Param("productId")String productId);
	//获取首页轮播
	public List<Productlist> selectswiper();
	//增加浏览量
	public void addproductpageviews(@Param("productId")String productId);
	//热门
	public List<Productlist> selecthotpro();
	//新品
	public List<Productlist> selectnewpro();
	public List<String> selectprotype();
}
