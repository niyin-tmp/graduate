package xyz.niyin.graduate.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.Productimgs;
import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.entity.Store;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.mapper.StoreMapper;
import xyz.niyin.graduate.utils.BosUtil;
import xyz.niyin.graduate.utils.JWTUtils;
import xyz.niyin.graduate.utils.Result;

@RequestMapping("/Store")
@RestController
public class StoreController {
	@Autowired
	StoreMapper storeMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	//校验用户是否是商家
	@GetMapping("/verifyStore")  
	@ResponseBody
	public Result verifyStore(String token) {
		JSONObject data = new JSONObject();
		System.out.println(token);
		DecodedJWT verify = JWTUtils.getTokenInfo(token);
		String AdminOpenid=verify.getClaim("openid").asString();
		System.out.println(AdminOpenid);
	
		System.out.println(storeMapper.selectStore(AdminOpenid));
		Store store=storeMapper.selectStore(AdminOpenid);
		if(store!=null) {
			data.put("data", "1");	
		}else {
			data.put("data", "0");	
		}
		
		Result result=new Result();
		return result.sussess(data);
		
	}
	@GetMapping("/getaddress")  
	@ResponseBody
	public Result getaddress(
			String token
			) {
		Result result=new Result();
		System.out.println(token);
		DecodedJWT verify=JWTUtils.getTokenInfo(token);
		String AdminOpenid=verify.getClaim("openid").asString();
		Store store =storeMapper.selectStore(AdminOpenid);
		return result.sussess(store);
	}
	@GetMapping("/setaddress")  
	@ResponseBody
	public Result setaddress(
			String token,double latitude,double longitude,String address
			) {
		System.out.println(token);
		DecodedJWT verify=JWTUtils.getTokenInfo(token);
		String AdminOpenid=verify.getClaim("openid").asString();
		System.out.println(token);
		System.out.println(latitude);
		System.out.println(longitude);
		System.out.println(address);
		storeMapper.setaddress(AdminOpenid, latitude, longitude, address);
		Result result=new Result();
		return result.sussess("ok");
	}
	@GetMapping("/getnearby")  
	@ResponseBody
	public Result getnearby(double latitude,double longitude) {
		JSONObject data = new JSONObject();
		//double latitude=22.995083;
		//double longitude=113.783088;
        //先计算查询点的经纬度范围
        double r = 6371;//地球半径千米
        double dis = 500;//0.5千米距离
        double dlng =  2*Math.asin(Math.sin(dis/(2*r))/Math.cos(latitude*Math.PI/180));
        dlng = dlng*180/Math.PI;//角度转为弧度
        double dlat = dis/r;
        dlat = dlat*180/Math.PI;      
        double minlat =latitude-dlat;
        double maxlat = latitude+dlat;
        double minlng = longitude -dlng;
        double maxlng = longitude + dlng;
        String aminlat=String.format("%.6f", minlat);
        String amaxlat=String.format("%.6f", maxlat);
        String aminlng=String.format("%.6f", minlng);
        String amaxlng=String.format("%.6f", maxlng);
		System.out.println("minlat:"+aminlat);
		System.out.println("maxlat"+amaxlat);
		System.out.println("minlng"+aminlng);
		System.out.println("maxlng"+amaxlng);
		Result result=new Result();
		List<Store> arr=storeMapper.selectnearby(minlat, maxlat, minlng, maxlng);
		JSONArray result1=new JSONArray();
		for(int i=0;i<arr.size();i++) {
			System.out.println(arr.get(i).getId());
			int storeid=arr.get(i).getId();
			List<Productlist> productlists= productlistMapper.selectProduct(storeid);
			
			if(productlists.size()!=0) {
			for(int k=0;k<productlists.size();k++) {
				JSONObject jsonObjectData = new JSONObject();
				System.out.println(productlists.get(k));
				jsonObjectData.put("ProductId",productlists.get(k).getProductId());
				jsonObjectData.put("ProductName",productlists.get(k).getProductName());
				jsonObjectData.put("ProductNum",productlists.get(k).getProductNum());
				jsonObjectData.put("ProductPrice",productlists.get(k).getProductPrice());
				jsonObjectData.put("ProductType",productlists.get(k).getProductType());
				jsonObjectData.put("ProductExplain",productlists.get(k).getProductExplain());
				//放图片
				List<Productimgs> lists=productimgsMapper.selectProductimg(productlists.get(k).getProductId());
				JSONArray jsonArray1=new JSONArray();
				JSONArray jsonArray2=new JSONArray();
				BosUtil bosUtil=new BosUtil();
				for(int j=0;j<lists.size();j++) {
					//JSONObject jsonObjectDatas = new JSONObject();
					//System.out.println();
					//System.out.println(lists.get(j));
					//获取文件类型
					String tyString=lists.get(j).getImgtype();
					//精选照片
					if(tyString.equals("0")) {
						jsonArray1.add(bosUtil.getimgurl(lists.get(j).getImgname()));
					}
					//详情照片
					if(tyString.equals("1")) {
						jsonArray2.add(bosUtil.getimgurl(lists.get(j).getImgname()));
					}
					//System.out.println("文件路径==="+bosUtil.getimgurl(lists.get(j).getImgname()));
				}
				//jsonObjectData.put("ProductPrice",list.get(i).getProductPrice());
				//精选图片
				jsonObjectData.put("productImgurlSift",jsonArray1);
				 //详情图片
				jsonObjectData.put("productImgurlDetails",jsonArray2);
				//System.out.println(lists);
				result1.add(jsonObjectData);
			}
			
			}
			//System.out.println(productlistMapper.selectProduct(storeid));
		}
		return result.sussess(result1);
	}
	@RequestMapping("/getstoreinfo")
	public JSONObject getstoreinfo(String token,int storeid) {
		JSONObject data=new JSONObject();
		System.out.println(token);
		System.out.println(storeid);
		String id=storeid+"";
		System.out.println(storeMapper.selectStoreid(id));
		Store storinfo=storeMapper.selectStoreid(id);
		List<Productlist> prolist= productlistMapper.selectProduct(storeid);
		JSONObject data1=new JSONObject();
		data1.put("storeinfo",storinfo);
		JSONArray proArray=new JSONArray();
		BosUtil bosUtil=new BosUtil();
		for(int i=0;i<prolist.size();i++) {
			JSONObject obj=new JSONObject();
			
			System.out.println(prolist.get(i));
			Productlist proinfo=prolist.get(i);
			String productId = prolist.get(i).getProductId();
			String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
			System.out.println(bosUtil.getimgurl(imgname));
			String logo=bosUtil.getimgurl(imgname);
			obj.put("proinfo",proinfo);
			obj.put("logo",logo);
			proArray.add(obj);
		}
		data1.put("proArray",proArray);
		data.put("code",0);
		data.put("data",data1);
		data.put("message","");
		return data;
	}
	//获取商家地址
	@RequestMapping("/getstoreaddress")
	public JSONObject getstoreaddress(String token) {
		JSONObject data =new JSONObject();
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String AdminOpenid=claims.get("openid").asString();
		storeMapper.selectStore(AdminOpenid);
		System.out.println(storeMapper.selectStore(AdminOpenid));
		data.put("code",0);
		data.put("data",storeMapper.selectStore(AdminOpenid));
		data.put("message","");
		return data;
	}
	//设置商家地址
	//获取商家地址
	@RequestMapping("/setstoreaddress")
	public JSONObject setstoreaddress(String token,double latitude,double longitude,String address) {
		JSONObject data =new JSONObject();
		System.out.println(token);
		System.out.println(latitude);
		System.out.println(longitude);
		System.out.println(address);
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String AdminOpenid=claims.get("openid").asString();
		storeMapper.setaddress(AdminOpenid, latitude, longitude, address);
		data.put("code",0);
		data.put("data","ok");
		data.put("message","");
		return data;
	}
	
}
