package xyz.niyin.graduate.utils;

import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

public class JWTUtils {
	private static final String SING="ljhlkhblkjb";
	//生成token
	public static String getToken(Map<String, String > map) {
		//设置过期时间
		Calendar instance=Calendar.getInstance();
		instance.add(Calendar.MINUTE, 300);
		
		//创建jwt builder
		JWTCreator.Builder builder=JWT.create();
		//payload
		for(Entry<String, String> entry:map.entrySet()) {
			builder.withClaim(entry.getKey(),entry.getValue());
		}
		String token=builder.withExpiresAt(instance.getTime())
				.sign(Algorithm.HMAC256(SING));
		return token;
	}
	
	//验证token
	public static void verify(String token) {
		JWT.require(Algorithm.HMAC256(SING)).build().verify(token);
	}
	//获取信息
	public static DecodedJWT getTokenInfo(String token) {
		return JWT.require(Algorithm.HMAC256(SING)).build().verify(token);
	}
}
