package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class Adminchat {
private int id;
private String useropenid;
private String username;
private String content;
private String contype;
private String status; 
private String userstatus;
}
