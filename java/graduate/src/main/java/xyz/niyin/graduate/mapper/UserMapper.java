package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import xyz.niyin.graduate.entity.User;

@Repository
public interface UserMapper {
public List<User> selectUserlist();
public User selectUser(@Param("openid")String openid);
public void insertUser(@Param("openid")String openid,@Param("nickName")String nickName,@Param("avatarUrl")String avatarUrl);
public void updatephone(@Param("openid")String openid,@Param("phone")String phone);
public void relievephone(@Param("openid")String openid);
//phone=#{phone},email=#{email},password=#{password},remark=#{remark}
public void updateadminuser(@Param("openId")String openId,
							@Param("phone")String phone,
							@Param("email")String email,
							@Param("password")String password,
							@Param("remark")String remark
							);

}
