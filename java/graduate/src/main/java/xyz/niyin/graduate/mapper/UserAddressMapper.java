package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import xyz.niyin.graduate.entity.UserAddress;


@Repository
public interface UserAddressMapper {
public List<UserAddress> selectaddress(@Param("openid")String openid);
public void setuseraddress(@Param("openid")String openid,@Param("status")String status,@Param("name")String name,@Param("phone")String phone,@Param("address")String address);
public void deleteaddress(@Param("openid")String openid,@Param("id")int id); 
public void updateaddressa(@Param("openid")String openid); 
public void updateaddressb(@Param("openid")String openid,@Param("id")int id); 
}
