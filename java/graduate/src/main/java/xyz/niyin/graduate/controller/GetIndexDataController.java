package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


import xyz.niyin.graduate.entity.IndexNav;
import xyz.niyin.graduate.entity.Productimgs;
import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.IndexNavMapper;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.utils.BosUtil;

@RequestMapping("/indexdata")
@RestController
public class GetIndexDataController {
	
	@Autowired
	IndexNavMapper indexNavMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	//@Autowired
	//GetSwiperListMapper getSwiperListMapper;
	//设置首页轮播图
	@RequestMapping("/getSwiperList")
	public JSONObject getSwiperList() {
		JSONObject retunObject=new JSONObject();
		JSONArray data=new JSONArray();
		List<Productlist> arr=productlistMapper.selectswiper();
		for(int i=0;i<arr.size();i++) {
			JSONObject resultJsonObject=new JSONObject();
			arr.get(i).getProductId();
			String productId=arr.get(i).getProductId();
			String imgname=productimgsMapper.selectProductimg(productId).get(0).getImgname();
			BosUtil bosUtil=new BosUtil();
			bosUtil.getimgurl(imgname);
			resultJsonObject.put("productId", productId);
			resultJsonObject.put("image_src", bosUtil.getimgurl(imgname));
			resultJsonObject.put("navigator_url", "/pages/productinfo/productinfo?"+"productId="+productId);
			resultJsonObject.put("open_type", "navigate");
			data.add(resultJsonObject);
		}
		retunObject.put("code", 0);
		retunObject.put("data", data);
		retunObject.put("message", "");
		//System.out.println(data);
		return retunObject;
	}
	//设置导航栏
	@RequestMapping("/getNavList")
	public JSONObject getNavList() {
		JSONObject retunObject=new JSONObject();
		List<IndexNav> arr= indexNavMapper.selectindexnav();
		JSONArray data=new JSONArray();
		BosUtil bosUtil=new BosUtil();
		for(int i=0;i<arr.size();i++) {
			JSONObject resultJsonObject=new JSONObject();
			//System.out.println(arr.get(i).getImage_name());
			resultJsonObject.put("image_name", arr.get(i).getImage_name());
			resultJsonObject.put("type_name", arr.get(i).getType_name());
			resultJsonObject.put("navigator_url", arr.get(i).getNavigator_url());
			resultJsonObject.put("img_url", bosUtil.getimgurls("nav-img", arr.get(i).getImage_name()));
			//bosUtil.getimgurls("nav-img", "hot.png");
			data.add(resultJsonObject);
		}
		//System.out.println(bosUtil.getimgurls("nav-img", "hot.png"));
		retunObject.put("code", 0);
		retunObject.put("data", data);
		retunObject.put("message", "");
	
		return retunObject;
	}
	//获取商品
	@RequestMapping("/getpro")
	public JSONObject getpro() {
		JSONObject retunObject=new JSONObject();
		BosUtil bosUtil=new BosUtil();
		List<Productlist> arr=productlistMapper.selectProducts();
		JSONArray data=new JSONArray();
		
		for(int i=0;i<arr.size();i++) {
			JSONObject resultJsonObject=new JSONObject();
			resultJsonObject.put("page_url","/pages/productinfo/productinfo?");
			resultJsonObject.put("productId",arr.get(i).getProductId());
			resultJsonObject.put("storeid",arr.get(i).getStoreid());
			resultJsonObject.put("productName",arr.get(i).getProductName());
			resultJsonObject.put("productNum",arr.get(i).getProductNum());
			resultJsonObject.put("productPrice",arr.get(i).getProductPrice());	
			resultJsonObject.put("roductType",arr.get(i).getProductType());
			resultJsonObject.put("productExplain",arr.get(i).getProductExplain());
			resultJsonObject.put("pageviews",arr.get(i).getPageviews());	
			List<Productimgs> arr1=productimgsMapper.selectProductimg(arr.get(i).getProductId());
			JSONArray data1=new JSONArray();
			for(int j=0;j<arr1.size();j++) {
				if(arr1.get(j).getImgtype().equals("0")) {
					//System.out.println("精选图："+arr1.get(j).getImgname());
					//bosUtil.getimgurl(arr1.get(j).getImgname());
					//System.out.println("url:"+bosUtil.getimgurl(arr1.get(j).getImgname()));
					data1.add(bosUtil.getimgurl(arr1.get(j).getImgname()));
				}
			}
			resultJsonObject.put("img_urls",data1);	
			data.add(resultJsonObject);
		}
		retunObject.put("code", 0);
		retunObject.put("data", data);
		retunObject.put("message", "");
		return retunObject;
		
	}
	
}
