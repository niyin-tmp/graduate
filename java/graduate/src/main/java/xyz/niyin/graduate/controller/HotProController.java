package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.ProductimgsMapper;

import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.utils.BosUtil;



@RestController
@RequestMapping("/hot")
public class HotProController {
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	@RequestMapping("/gethotpro")
public JSONObject gethotpro() {
	JSONObject data=new JSONObject();
	productlistMapper.selecthotpro();
	//System.out.println(productlistMapper.selecthotpro());
	List<Productlist> arr=productlistMapper.selecthotpro();
	BosUtil bosUtil=new BosUtil();
	JSONArray array=new JSONArray();
	for (int i = 0; i < arr.size(); i++) {
		//System.out.println(arr.get(i).getProductId());
		JSONObject obj=new JSONObject();
		String productId=arr.get(i).getProductId();
		String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
		System.out.println(bosUtil.getimgurl(imgname));
		String logo=bosUtil.getimgurl(imgname);
		
		obj.put("proinfo",arr.get(i));
		obj.put("logo",logo);
		array.add(obj);
	}
	data.put("code",0);
	data.put("data",array);
	data.put("message","");
	return data;
}
}
