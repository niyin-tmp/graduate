package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.AdminuserMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.mapper.StoreMapper;
import xyz.niyin.graduate.mapper.UserMapper;

@RequestMapping("/pcupdateuserlist")
@RestController
public class PCupdateController {
	@Autowired
	UserMapper userMapper;
	@Autowired
	StoreMapper storeMapper;
	@Autowired
	AdminuserMapper adminuserMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/pcupdateuser")
public JSONObject pcupdateuser(int id,
		String openId,
		String nickName,
		String phone,
		String password,
		String email,
//		String avatarUrl,
		String remark 
		) {
	System.out.println(id);
	System.out.println(openId);
	JSONObject data=new JSONObject();
	userMapper.updateadminuser(openId, phone, email, password, remark);
	data.put("meta",400);
	data.put("data","ok");
	return data;
}

@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/pcupdatestore")
public JSONObject pcupdatestore(int id,
		String adminOpenid,
		String storeName,
		String storeType,
		String latitude,
		String longitude,
//		String avatarUrl,
		String address 
		) {
	System.out.println(id);
	System.out.println(adminOpenid);
	JSONObject data=new JSONObject();
	//userMapper.updateadminuser(openId, phone, email, password, remark);
	storeMapper.setaddminupdate(adminOpenid, storeName, storeType, latitude, longitude, address);
	data.put("meta",400);
	data.put("data","ok");
	return data;
}
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/pcupdatepro")
public JSONObject pcupdatepro(int id,
		String productId,
		String storeid,
		String productType,
		String productName,
		String productPrice,
		String productNum,
		int pageviews,
		String productExplain 
		) {
	System.out.println(id);
	System.out.println(pageviews);
	JSONObject data=new JSONObject();
	//userMapper.updateadminuser(openId, phone, email, password, remark);
	//storeMapper.setaddminupdate(adminOpenid, storeName, storeType, latitude, longitude, address);
	productlistMapper.updateadminProduct(productId, productName, productNum, productPrice, productType, pageviews, productExplain);
	data.put("meta",400);
	data.put("data","ok");
	return data;
}
//查询商品
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/selectproid")
public JSONObject selectproid(String uid,
		String uname,
		String utype
		) {
	JSONArray array=new JSONArray();
	System.out.println(uid);
	System.out.println(uname);
	//System.out.println(utype);
	String productId=uid;
	//String productName=uname;
	JSONObject data=new JSONObject();
	//System.out.println(productlistMapper.selectProductinfo(productId));
	Productlist proinfo=productlistMapper.selectProductinfo(productId);
	array.add(proinfo);
	System.out.println(array);
	//System.out.println(productlistMapper.selectProductsearch(productName));
		if(proinfo==null) {	
		//List<Productlist> prolist=productlistMapper.selectProductsearch(productName);
		data.put("meta",0);
		data.put("data",array);
		}else{
			data.put("meta",400);
			data.put("data",array);	
			}
	return data;
}
//查询商品
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/selectpro")
public JSONObject selectpro(
		String uname,
		String utype
		) {
		String productName=uname;
		JSONObject data=new JSONObject();
		String productType=utype;
		if(utype=="") {
			List<Productlist> prolist=productlistMapper.selectProductsearch(productName);
			if(prolist.size()==0) {
				data.put("meta",0);
				data.put("data",prolist);		
			}else {
				data.put("meta",400);
				data.put("data",prolist);	
			}
		}else {
			List<Productlist> prolist=productlistMapper.selectProductsearchtype(productType, productName);
			if(prolist.size()==0) {
				data.put("meta",0);
				data.put("data",prolist);		
			}else {
				data.put("meta",400);
				data.put("data",prolist);
			}
		}		
	return data;
}
//修改用户
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/updateadminuser")
public JSONObject updateadminuser(int id,
		String adminpassword,
		String adminusername,
		String email,
		String phone,
		String utype
		) {
	JSONObject data=new JSONObject();	
	adminuserMapper.updateadminuser(id, adminusername, adminpassword, utype, phone, email);
	data.put("meta",400);
	data.put("data","ok");
	return data;	
}
//addadminuser添加用户
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/addadminuser")
public JSONObject addadminuser(
		String adminpassword,
		String adminusername,
		String email,
		String phone,
		String utype
		) {
	System.out.println(adminpassword);
	JSONObject data=new JSONObject();	
	//adminuserMapper.updateadminuser(id, adminusername, adminpassword, utype, phone, email);
	adminuserMapper.addadminuser(adminusername, adminpassword, utype, phone, email);
	data.put("meta",400);
	data.put("data","ok");
	return data;	
}
//deleteadminuser
@CrossOrigin(origins = "*",maxAge = 3600)
@RequestMapping("/deleteadminuser")
public JSONObject deleteadminuser(
		int id
		) {
	System.out.println(id);
	JSONObject data=new JSONObject();	
	//adminuserMapper.updateadminuser(id, adminusername, adminpassword, utype, phone, email);
	//adminuserMapper.addadminuser(adminusername, adminpassword, utype, phone, email);
	adminuserMapper.deleteadminuser(id);
	data.put("meta",400);
	data.put("data","ok");
	return data;	
}
}
