package xyz.niyin.graduate.service;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import xyz.niyin.graduate.entity.Email;



public interface EmailService {
	public void setReceiveMailAccount(String receiveMailAccount);
	public void setInfo(String info);
	public  void Send() throws Exception;
	public  MimeMessage createMessage(Session session, String sendMail, String receiveMail,String info) throws Exception;	
	public Email selectEmail(String emailNo);
}
