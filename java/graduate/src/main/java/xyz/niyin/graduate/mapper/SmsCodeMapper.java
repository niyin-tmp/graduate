package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import xyz.niyin.graduate.entity.SmsCode;


@Repository
public interface SmsCodeMapper {
	public List<SmsCode> selectsmscode(@Param("phone")String phone, @Param("type")String type);
	public void addsmscode(@Param("token")String token,@Param("phone")String phone,@Param("code")String code,@Param("type")String type,@Param("settime")long settime);
	public void deletesmscode(@Param("phone")String phone,@Param("type")String type);
}
