package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.Adminchat;


public interface AdminchatMapper {
public List<Adminchat> selectAdminchatlist();
public List<Adminchat> selectuserchatlist(String useropenid);
public List<Adminchat> selectuser();
public void insertchat(@Param("useropenid")String useropenid,@Param("username")String username,
		@Param("content")String content,@Param("contype")String contype,@Param("status")String status,@Param("userstatus")String userstatus);


}
