package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import xyz.niyin.graduate.entity.Collect;


public interface CollectMapper {
	public void addcollect(@Param("openid")String openid,@Param("productId")String productId);
	public Collect selectcollect(@Param("openid")String openid, @Param("productId")String productId);
	public List<Collect> selectcollects(@Param("openid")String openid);
	public void deletecollect(@Param("openid")String openid,@Param("productId")String productId);
}
