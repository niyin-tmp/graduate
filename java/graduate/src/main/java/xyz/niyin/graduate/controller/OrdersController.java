package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.Orders;
import xyz.niyin.graduate.mapper.StoreMapper;
import xyz.niyin.graduate.utils.JWTUtils;
import xyz.niyin.graduate.mapper.OrderMapper;

@RequestMapping("/orders")
@RestController
public class OrdersController {
	@Autowired
	OrderMapper orderMapper;
	@Autowired
	StoreMapper storeMapper;
	@RequestMapping("/getorders")
	public JSONObject getorders(String token) {
		DecodedJWT verify= JWTUtils.getTokenInfo(token);
		String openid=verify.getClaim("openid").asString();
		System.out.println(token);
		orderMapper.selectOrders(openid);
		List<Orders> list=orderMapper.selectOrders(openid);
		System.out.println(list);

		JSONObject jsobj1=new JSONObject();
		jsobj1.put("code", 0);
		//jsobj1.put("data", reString);
		jsobj1.put("data", list);
		jsobj1.put("message", "");
		return jsobj1;
	}
	@RequestMapping("/getorderlist")
	public JSONObject getorderlist(String token) {
		DecodedJWT verify=JWTUtils.getTokenInfo(token);
		String AdminOpenid=verify.getClaim("openid").asString();
		System.out.println(token);
		System.out.println();
		String storeid=storeMapper.selectStore(AdminOpenid).getId()+"";
		System.out.println(orderMapper.selectOrderlist(storeid));
		List<Orders> orderList=orderMapper.selectOrderlist(storeid);
		JSONObject jsobj1=new JSONObject();
		jsobj1.put("code", 0);
		//jsobj1.put("data", reString);
		jsobj1.put("data", orderList);
		jsobj1.put("message", "");
		return jsobj1;
	}
	@RequestMapping("/storesetorder")
	public JSONObject setorder(String token,String ordernum) {
		JSONObject jsobj1=new JSONObject();
		DecodedJWT verify=JWTUtils.getTokenInfo(token);
		String openid=verify.getClaim("openid").asString();
		System.out.println(token);
		String status="2";
		orderMapper.setorderstatus(openid, ordernum, status);
		jsobj1.put("code", 0);
		//jsobj1.put("data", reString);
		jsobj1.put("data", "ok");
		jsobj1.put("message", "");
		return jsobj1;
	}
	
	@RequestMapping("/okorder")
	public JSONObject okorder(String token,String ordernum) {
		JSONObject jsobj1=new JSONObject();
		DecodedJWT verify=JWTUtils.getTokenInfo(token);
		String openid=verify.getClaim("openid").asString();
		System.out.println(token);
		String status="3";
		orderMapper.setorderstatus(openid, ordernum, status);
		jsobj1.put("code", 0);
		//jsobj1.put("data", reString);
		jsobj1.put("data", "ok");
		jsobj1.put("message", "");
		return jsobj1;
	}
	
	@RequestMapping("/orderdelete")
	public JSONObject orderdelete(String token,String ordernum) {
		JSONObject jsobj1=new JSONObject();
		DecodedJWT verify=JWTUtils.getTokenInfo(token);
		String openid=verify.getClaim("openid").asString();
		System.out.println(token);
		String status="4";
		orderMapper.setorderstatus(openid, ordernum, status);
		jsobj1.put("code", 0);
		//jsobj1.put("data", reString);
		jsobj1.put("data", "ok");
		jsobj1.put("message", "");
		return jsobj1;
	}
}
