package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.utils.BosUtil;


@RestController
@RequestMapping("/new")
public class NewProController {
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	@RequestMapping("/getnewpro")
	public JSONObject getnewpro() {
		JSONObject data=new JSONObject();
		
		System.out.println(productlistMapper.selectnewpro());
		List<Productlist> newarr=productlistMapper.selectnewpro();
		BosUtil bosUtil=new BosUtil();
		JSONArray array=new JSONArray();
		for (int i = 0; i < newarr.size(); i++) {
			JSONObject obj=new JSONObject();
			String productId=newarr.get(i).getProductId();
			String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
			System.out.println(bosUtil.getimgurl(imgname));
			String logo=bosUtil.getimgurl(imgname);
			System.out.println(newarr.get(i));
			obj.put("proinfo",newarr.get(i));
			obj.put("logo",logo);
			array.add(obj);
		}
		data.put("code",0);
		data.put("data",array);
		data.put("message","");
		return data;
	}
}
