package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
//购物车
public class ProCart {
	private int id;
	private String openid;
	private String productId;
}
