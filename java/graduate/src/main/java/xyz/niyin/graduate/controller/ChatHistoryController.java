package xyz.niyin.graduate.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import xyz.niyin.graduate.entity.ChatHistory;
import xyz.niyin.graduate.mapper.ChatHistoryMapper;
import xyz.niyin.graduate.mapper.StoreMapper;
import xyz.niyin.graduate.mapper.UserMapper;
import xyz.niyin.graduate.utils.JWTUtils;

@RestController
@RequestMapping("/ChatHistory")
public class ChatHistoryController {
	
	@Autowired
	UserMapper userMapper;
	@Autowired
	ChatHistoryMapper chatHistoryMapper;
	@Autowired
	StoreMapper storeMapper;
	//用户发送信息
@RequestMapping("/setchathistory")
public JSONObject setchathistory(String token,String content,String storeid,String contype) {
	System.out.println(token);
	//拿到用户标识
	DecodedJWT ok= JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String useropenid=claims.get("openid").asString();
	//拿到商家标识
	System.out.println(useropenid);
	System.out.println("----");
	storeMapper.selectStoreid(storeid);
	String storeopenid=storeMapper.selectStoreid(storeid).getAdminOpenid();
	System.out.println(storeopenid);
	if(content!="") {
		//执行插入
		System.out.println("ffffffffffff");
		String status="0";
		String userstatus="1";
		//String contype="0";
		chatHistoryMapper.insertchat(useropenid, storeopenid, content, status,userstatus, contype);
	}
	System.out.println(storeid);
	
	JSONObject data=new JSONObject();
	
	data.put("code", 0);
	data.put("data", "ok");
	data.put("message", "");
	return data;
}
//商家发送信息
@RequestMapping("/setstorechathistory")
public JSONObject setstorechathistory(String token,String content,String useropenid,String contype) {
	//拿到用户标识
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String storeopenid=claims.get("openid").asString();

	if(content!="") {
		//执行插入
		System.out.println("ffffffffffff");
		String status="1";
		String userstatus="0";
		//String contype="0";
		chatHistoryMapper.insertchat(useropenid, storeopenid, content, status,userstatus, contype);
	}
	JSONObject data=new JSONObject();
	
	data.put("code", 0);
	data.put("data", "ok");
	data.put("message", "");
	return data;
}
//获取聊条记录
@RequestMapping("/getchathistory")
public JSONObject getchathistory(String token,String storeid) {
	JSONObject data=new JSONObject();
	if(storeid!="") {
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String useropenid=claims.get("openid").asString();
		String storeopenid=storeMapper.selectStoreid(storeid).getAdminOpenid();
		System.out.println(storeopenid);
		chatHistoryMapper.selectchat(useropenid, storeopenid);
		System.out.println(chatHistoryMapper.selectchat(useropenid, storeopenid));
		data.put("code", 0);
		data.put("data", chatHistoryMapper.selectchat(useropenid, storeopenid));
		data.put("message", "");
	}else {
		data.put("code", 0);
		data.put("data","");
		data.put("message", "");
	}
	return data;
}
@RequestMapping("/getstorechathistory")
public JSONObject getstorechathistory(String token,String useropenid) {
	JSONObject data=new JSONObject();
	if(useropenid!="") {
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String storeopenid=claims.get("openid").asString();
		System.out.println(storeopenid);
		chatHistoryMapper.selectchat(useropenid, storeopenid);
		System.out.println(chatHistoryMapper.selectchat(useropenid, storeopenid));
		data.put("code", 0);
		data.put("data", chatHistoryMapper.selectchat(useropenid, storeopenid));
		data.put("message", "");
	}else {
		data.put("code", 0);
		data.put("data","");
		data.put("message", "");
	}
	return data;
}
//用户获取聊天商家
@RequestMapping("/getstorelist")
public JSONObject getstorelist(String token) {
	JSONObject data=new JSONObject();
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String useropenid=claims.get("openid").asString();
	chatHistoryMapper.selectstore(useropenid);
	System.out.println(chatHistoryMapper.selectstore(useropenid));
	List<ChatHistory> arr=chatHistoryMapper.selectstore(useropenid);
	JSONArray array=new JSONArray();
	for (int i = 0; i < arr.size(); i++) {
		JSONObject obj=new JSONObject();
		String AdminOpenid= arr.get(i).getStoreopenid();
		storeMapper.selectStore(AdminOpenid);
		//System.out.println(storeMapper.selectStore(AdminOpenid).getStoreName());
		String storeName=storeMapper.selectStore(AdminOpenid).getStoreType();
		int storeid=storeMapper.selectStore(AdminOpenid).getId();
		String storeopenid=AdminOpenid;
		List<ChatHistory> historylist=chatHistoryMapper.selectuserstatus(useropenid, storeopenid);

		System.out.println(historylist.size());
		obj.put("storeid",storeid);
		obj.put("num",historylist.size());
		obj.put("storeName",storeName);
		array.add(obj);
	}
	data.put("code", 0);
	data.put("data",array);
	data.put("message", "");
	return data;
}
//获取聊天记录总数
@RequestMapping("/gethistorynum")
public JSONObject gethistorynum(String token) {
	JSONObject data=new JSONObject();
	//System.out.println(token);
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String storeopenid=claims.get("openid").asString();
	//chatHistoryMapper.selectuser(storeopenid);
	//System.out.println(chatHistoryMapper.selectuser(storeopenid));
	List<ChatHistory> arr1=chatHistoryMapper.selectuser(storeopenid);
	JSONArray array=new JSONArray();
	for (int i = 0; i < arr1.size(); i++) {
		JSONObject obj=new JSONObject();
		String useropenid=arr1.get(i).getUseropenid();
		String openid=useropenid;
		String userName=userMapper.selectUser(openid).getNickName();
		//System.out.println(arr1.get(i).getUseropenid());
		List<ChatHistory> arr2=chatHistoryMapper.selectstatus(useropenid, storeopenid);
		int num=arr2.size();
		//System.out.println(arr2.size());
		obj.put("useropenid",useropenid);
		obj.put("userName",userName);
		obj.put("num",num);
		array.add(obj);
	}
	data.put("code", 0);
	data.put("data",array);
	data.put("message", "");
return data;
}
//消除用户未读标志
@RequestMapping("/usersetemp")
public JSONObject usersetemp(String token,String storeid) {
	JSONObject data=new JSONObject();
	System.out.println(token);
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String useropenid=claims.get("openid").asString();
	System.out.println(storeMapper.selectStoreid(storeid).getAdminOpenid());
	
	String storeopenid=storeMapper.selectStoreid(storeid).getAdminOpenid();
	chatHistoryMapper.updateuserstatus(useropenid, storeopenid);
	//System.out.println(useropenid);
	data.put("code", 0);
	data.put("data","ok");
	data.put("message", "");
return data;
}
//移除未读标志
@RequestMapping("/setemp")
public JSONObject setemp(String token,String useropenid) {
	JSONObject data=new JSONObject();
	System.out.println(token);
	DecodedJWT ok=JWTUtils.getTokenInfo(token);
	Map<String, Claim> claims=ok.getClaims();
	String storeopenid=claims.get("openid").asString();
	chatHistoryMapper.updatestatus(useropenid, storeopenid);
	System.out.println(useropenid);
	data.put("code", 0);
	data.put("data","ok");
	data.put("message", "");
return data;
}
}
