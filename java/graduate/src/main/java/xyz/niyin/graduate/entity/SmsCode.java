package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class SmsCode {
	private int Id;
	private String token;
	private String phone;
	private String code;
	private String type;
	private long settime;
}
