package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class UserAddress {
	private int id;

    private String openId;

    private String status;
    
    private String name;
    
    private String phone;
    
    private String address;
}
