package xyz.niyin.graduate.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import xyz.niyin.graduate.entity.*;
import xyz.niyin.graduate.mapper.*;
import xyz.niyin.graduate.utils.BosUtil;
import xyz.niyin.graduate.utils.JWTUtils;


@RequestMapping("/productinfo")
@RestController
public class ProductInfoController {
	@Autowired
	ProductimgsMapper productimgsMapper;
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	StoreMapper storeMapper;
	@Autowired
	ProCartMapper proCartMapper;
	@Autowired
	CollectMapper collectMapper;
	@Autowired
	HistoryMapper historyMapper;
	//增加浏览次数
	@RequestMapping("/addPageviges")
	public JSONObject addPageviges(String productId) {
		JSONObject retunObject = new  JSONObject();
		//System.out.println(productId);
		productlistMapper.addproductpageviews(productId);
		retunObject.put("code", 0);
		retunObject.put("data", "ok");
		retunObject.put("message", "");
		//System.out.println(data);
		return retunObject;
	}
	//增加历史记录
	
	
	//获取商品信息
	@RequestMapping("/getproductinfo")
	public JSONObject getproductinfo(
		@RequestParam(value = "token", required = false)String token,
		@RequestParam(value = "productId", required = false)String productId) {
		JSONObject retunObject = new  JSONObject();
		JSONObject data = new  JSONObject();
		JSONArray arra = new  JSONArray();
		JSONArray arrb = new  JSONArray();
		DecodedJWT ok= JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		History history= historyMapper.selectHistory(openid, productId);
		if(history==null) {
			System.out.println("null==========");
			historyMapper.insertHistory(openid, productId);
		}
		
		
		//获取到商品详细信息
		Productlist Productinfo= productlistMapper.selectProductinfo(productId);
		//System.out.println(productlistMapper.selectProductinfo(productId));
		List<Productimgs>  proimgList=productimgsMapper.selectProductimg(productId);
		BosUtil bosUtil=new BosUtil();
		//System.out.println(proimgList);
		for(int i=0;i<proimgList.size();i++) {
			//System.out.println(proimgList.get(i));
			//获取到精选图
			if(proimgList.get(i).getImgtype().equals("0")) {
				arra.add(bosUtil.getimgurl(proimgList.get(i).getImgname()));
			}
			//获取到详情图
			if(proimgList.get(i).getImgtype().equals("1")) {
				arrb.add(bosUtil.getimgurl(proimgList.get(i).getImgname()));
			}
			
		}
		//System.out.println(arra);
		//System.out.println(arrb);
		data.put("productinfo",productlistMapper.selectProductinfo(productId));
		data.put("arra",arra);
		data.put("arrb",arrb);
		//查询是否已收藏

		Collect collect= collectMapper.selectcollect(openid, productId);
		if(collect==null) {
			data.put("collect","0");
		}else {
			data.put("collect","1");
		}
		retunObject.put("code", 0);
		retunObject.put("data", data);
		retunObject.put("message", "");
		//System.out.println(data);
		return retunObject;
	}
	
	//收藏
		@RequestMapping("/addcollect")
		public JSONObject addcollect(
				@RequestParam(value = "token", required = false)String token,
				@RequestParam(value = "productId", required = false)String productId
				) {
			System.out.println(token);
			System.out.println(productId);
			JSONObject retunObject=new JSONObject();
			DecodedJWT ok=JWTUtils.getTokenInfo(token);
			Map<String, Claim> claims=ok.getClaims();
			String openid=claims.get("openid").asString();
			Collect collect= collectMapper.selectcollect(openid, productId);
			if(collect==null) {
				collectMapper.addcollect(openid, productId);
			}
			retunObject.put("code", 0);
			retunObject.put("data", "ok");
			retunObject.put("message", "");
			//System.out.println(data);
			return retunObject;
		}
		//移除收藏
		@RequestMapping("/deletecollect")
		public JSONObject deletecollect(
				@RequestParam(value = "token", required = false)String token,
				@RequestParam(value = "productId", required = false)String productId
				) {
			System.out.println(token);
			System.out.println(productId);
			JSONObject retunObject=new JSONObject();
			DecodedJWT ok=JWTUtils.getTokenInfo(token);
			Map<String, Claim> claims=ok.getClaims();
			String openid=claims.get("openid").asString();
			collectMapper.deletecollect(openid, productId);

			retunObject.put("code", 0);
			retunObject.put("data", "ok");
			retunObject.put("message", "");
			//System.out.println(data);
			return retunObject;
		}
		
	//加入购物车
	@RequestMapping("/addproductcart")
	public JSONObject addproductcart(
			@RequestParam(value = "token", required = false)String token,
			@RequestParam(value = "productId", required = false)String productId
			) {
		System.out.println(token);
		System.out.println(productId);
		JSONObject retunObject=new JSONObject();
		DecodedJWT ok=JWTUtils.getTokenInfo(token);
		Map<String, Claim> claims=ok.getClaims();
		String openid=claims.get("openid").asString();
		ProCart proCart=proCartMapper.selectcart(openid, productId);
		if(proCart==null) {
			proCartMapper.addprocart(openid, productId);
		}
		
		
		retunObject.put("code", 0);
		retunObject.put("data", "ok");
		retunObject.put("message", "");
		//System.out.println(data);
		return retunObject;
	}
}
