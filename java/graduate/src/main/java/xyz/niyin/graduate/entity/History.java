package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class History {
	private int id;
    private String openid;
    private String productId;
}
