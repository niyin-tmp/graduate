package xyz.niyin.graduate.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.bos.BosClient;
import com.baidubce.services.bos.BosClientConfiguration;
import com.baidubce.services.bos.model.PutObjectResponse;

import lombok.Data;
import xyz.niyin.graduate.utils.Result;

@Data
@RestController
@ConfigurationProperties("bos.config")
@RequestMapping("/apis")
public class TestUpController {
	private String ACCESS_KEY_ID;
	private String SECRET_ACCESS_KEY;
	private String ENDPOINT;
@RequestMapping("con")
public void Hello() {
	System.out.println(ACCESS_KEY_ID);
	System.out.println(SECRET_ACCESS_KEY);
}
@RequestMapping(value="/getstring", method = RequestMethod.POST)
public String mk(@RequestParam("uploadFile") MultipartFile uploadFile) throws IOException {
		    //String ACCESS_KEY_ID = "a6485808392b47b29e4df8ed9d19fa56";             // 用户的Access Key ID
	        //String SECRET_ACCESS_KEY = "eecf2958a46145c4b739d1ed4beae8e7";         // 用户的Secret Access Key
	        //String ENDPOINT = "bj.bcebos.com";                                     // 用户自己指定的域名，参考说明文档

	        // 初始化一个BosClient
	        BosClientConfiguration config = new BosClientConfiguration();
	        config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
	        config.setEndpoint(ENDPOINT);
	        
	        BosClient client = new BosClient(config);

	        // 获取指定文件
	        //File file = new File("D://文本文档.rar");
	        // 获取数据流
	        String fileName = uploadFile.getOriginalFilename();
	    
	        //获取输入流 
	        InputStream is = uploadFile.getInputStream();
	     /// InputStream inputStream = new FileInputStream("D://新建文本文档.rar");
	    //  byte[] byte0 = new byte[0];
  
	        // 以文件形式上传Object
	        //PutObjectResponse putObjectFromFileResponse =
	        //     client.putObject("javawx", "新建文本文档.rar", file);
	        // 以数据流形式上传Object
	        PutObjectResponse putObjectResponseFromInputStream =
	               client.putObject("javawx", fileName, is);
	        // 以二进制串上传Object
	        //PutObjectResponse putObjectResponseFromByte =
	             //   client.putObject("bucketName", "byte-objectKey", byte0);
	        // 以字符串上传Object
	       // PutObjectResponse putObjectResponseFromString =
	            //    client.putObject("bucketName", "string-objectKey", "hello world");

	        // 打印ETag
	        //System.out.println(putObjectFromFileResponse.getETag());
	        System.out.println(putObjectResponseFromInputStream.getETag());
	   	    URL url = client.generatePresignedUrl("javawx",fileName,100);
	   	    System.out.println(url.toString());
	      //  System.out.println(putObjectResponseFromByte.getETag());
	       // System.out.println(putObjectResponseFromString.getETag());
	        // 关闭客户端
	        client.shutdown();
	return "helloworld";	
}
@RequestMapping("/geturl")
public Result urlString() {
	 String ACCESS_KEY_ID = "xxx";             // 用户的Access Key ID
     String SECRET_ACCESS_KEY = "xxx";         // 用户的Secret Access Key
     String ENDPOINT = "javawx.bj.bcebos.com";                                     // 用户自己指定的域名，参考说明文档
     Result result =new Result();
     // 初始化一个BosClient
     BosClientConfiguration config = new BosClientConfiguration();
     config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
     config.setEndpoint(ENDPOINT);
     BosClient client = new BosClient(config);
	 URL url = client.generatePresignedUrl("javawx","1.png",100);
	   //指定用户需要获取的Object所在的Bucket名称、该Object名称、时间戳、URL的有效时长   
	 return result.sussess(url.toString());
}
}
