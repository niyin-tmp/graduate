package xyz.niyin.graduate.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import xyz.niyin.graduate.entity.Adminuser;


@Repository
public interface AdminuserMapper {
public List<Adminuser> selectAdminuserlist();
public Adminuser selectAdminlogin(@Param("username")String username,@Param("password")String password);
public void updateadminuser(
		@Param("id")int id,
		@Param("adminusername")String adminusername,
		@Param("adminpassword")String adminpassword,
		@Param("utype")String utype,
		@Param("phone")String phone,
		@Param("email")String email
		);
public void addadminuser(		
		@Param("adminusername")String adminusername,
		@Param("adminpassword")String adminpassword,
		@Param("utype")String utype,
		@Param("phone")String phone,
		@Param("email")String email);
public void deleteadminuser(	@Param("id")int id);
}

