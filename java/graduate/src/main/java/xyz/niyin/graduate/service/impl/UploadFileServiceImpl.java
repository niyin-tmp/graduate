package xyz.niyin.graduate.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.bos.BosClient;
import com.baidubce.services.bos.BosClientConfiguration;
import com.baidubce.services.bos.model.PutObjectResponse;

import lombok.Data;
import xyz.niyin.graduate.service.UploadFileService;

@Data
@Service
@ConfigurationProperties("bos.config")
public class UploadFileServiceImpl implements UploadFileService {
	private String ACCESS_KEY_ID;
	private String SECRET_ACCESS_KEY;
	private String ENDPOINT;
	//上传图片
	@Override
	public String mk(MultipartFile uploadFile,String imgname) throws IOException {
        // 初始化一个BosClient
        BosClientConfiguration config = new BosClientConfiguration();
        config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
        config.setEndpoint(ENDPOINT);
        BosClient client = new BosClient(config);
        //获取输入流 
        InputStream is = uploadFile.getInputStream();
        // 以数据流形式上传Object
        PutObjectResponse putObjectResponseFromInputStream =
               client.putObject("photo-reports", imgname, is);
   	    URL url = client.generatePresignedUrl("photo-reports",imgname,100);
   	    System.out.println(url.toString());
   	    String urlString=url.toString();
		return urlString;
	}
	//删除图片
	public String deleteimg(String imgname) {
		System.out.println("impl");
		System.out.println(imgname);
        // 初始化一个BosClient
        BosClientConfiguration config = new BosClientConfiguration();
        config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
        config.setEndpoint(ENDPOINT);
        
        BosClient client = new BosClient(config);
        client.deleteObject("photo-reports", imgname);
		return null;
		
	}

}
