package xyz.niyin.graduate.service.impl;

import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


import xyz.niyin.graduate.entity.User;
import xyz.niyin.graduate.mapper.UserMapper;
import xyz.niyin.graduate.service.UserService;
import xyz.niyin.graduate.utils.JWTUtils;
import xyz.niyin.graduate.utils.Result;
import xyz.niyin.graduate.utils.WechatUtil;

@Service
public class UserServiceImpl implements UserService {
@Autowired
UserMapper userMapper;
//微信授权登录
@Override
public Result Wxlogin(String code, String rawData) {
	JSONObject jsobj = new JSONObject();
	JSONObject data = new JSONObject();
	Result result=new Result();
	try {
		JSONObject SessionKeyOpenId = WechatUtil.getSessionKeyOrOpenId(code);
		//获得用户唯一标识
		 String openid = SessionKeyOpenId.getString("openid");
		 System.out.println(openid);
	    //String sessionKey = SessionKeyOpenId.getString("session_key");
		//把用户非敏感信息转为json格式
	    JSONObject rawDataJson = JSON.parseObject(rawData);
	    System.out.println("1111");
	    System.out.println(rawDataJson);
	    String nickName=rawDataJson.getString("nickName");
	    String avatarUrl=rawDataJson.getString("avatarUrl");
	    System.out.println(nickName);
	    System.out.println(avatarUrl);
	    //查询数据库是否已存在用户信息
	    User user =userMapper.selectUser(openid);
	    //用户信息入库
	    if(user==null) {
	    	//System.out.println("用户信息不存在，正在添加到数据库");
	    	 userMapper.insertUser(openid, nickName, avatarUrl);	 
	    } else {
	    	//System.out.println("数据库存在用户信息");
		    nickName=user.getNickName();
		    avatarUrl=user.getAvatarUrl();
		}
	    User userid=userMapper.selectUser(openid);
	    String id=userid.getId()+"";
		//设置token
		Map<String , String> map = new HashedMap();
		map.put("id",id);
		map.put("openid", openid);
		map.put("nickName", nickName);
		map.put("avatarUrl", avatarUrl);
		String token= JWTUtils.getToken(map);
		System.out.println(token);
		data.put("token", token);
		data.put("avatarUrl",avatarUrl);
		data.put("nickName", nickName);
		//System.out.println(token.toString());
		jsobj.put("code", 0);
		jsobj.put("data", data);
		jsobj.put("message", "");
		return result.sussess(data);	
	} catch (Exception e) {
		// TODO: handle exception
		return result.error();
	}
		
}

}
