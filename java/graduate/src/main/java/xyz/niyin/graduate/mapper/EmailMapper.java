package xyz.niyin.graduate.mapper;


import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import xyz.niyin.graduate.entity.Email;


@Repository
public interface EmailMapper {
 public Email selectEmail(@Param("emailNo")String emailNo);
}
