package xyz.niyin.graduate.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import xyz.niyin.graduate.entity.Email;
import xyz.niyin.graduate.service.EmailService;

@RequestMapping("/email")
@Controller
public class EmailController {
	@Autowired
	EmailService emailService;
@GetMapping("/v1")	
@ResponseBody
public String email(){

	        emailService.setReceiveMailAccount("279143004@qq.com");
	        //创建10位发验证码
			Random random=new Random();
			String str="";
			for(int i=0;i<6;i++) {
				int n=random.nextInt(10);
				str+=n;
			}
			emailService.setInfo(str);
			try {
				emailService.Send();
			} catch (Exception e) {
				e.printStackTrace();
			}
	return "ok";
}
@GetMapping("/v2")	
@ResponseBody
public JSONObject teString(String emailno,String emailpassword) {
	System.out.println("登录邮箱："+emailno);
	System.out.println("登录密码："+emailpassword);
	Email email=emailService.selectEmail(emailno);
	if(email!=null) {
		System.out.println(email.getEmailNo());
		System.out.println(email.getEmailpassword());
		if(emailno.equals(email.getEmailNo())&&emailpassword.equals(email.getEmailpassword())) {
			JSONObject jsobj1 = new JSONObject();
			JSONObject data = new JSONObject();
			data.put("data", "验证通过");
			jsobj1.put("code", 0);
			jsobj1.put("data", data);
			//jsobj1.put("openId","opid");
			jsobj1.put("message", "");
			return jsobj1;
		}else {
			JSONObject jsobj1 = new JSONObject();
			JSONObject data = new JSONObject();
			jsobj1.put("code", 1);
			jsobj1.put("data", data);
			//jsobj1.put("openId","opid");
			jsobj1.put("message", "请输入正确的邮箱和密码");
			return jsobj1;
		}
	}else {
		JSONObject jsobj1 = new JSONObject();
		JSONObject data = new JSONObject();
		jsobj1.put("code", 1);
		jsobj1.put("data", data);
		//jsobj1.put("openId","opid");
		jsobj1.put("message", "请输入正确的邮箱和密码");
		return jsobj1;
	}

}
}
