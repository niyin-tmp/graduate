package xyz.niyin.graduate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baidubce.services.cdn.model.JsonObject;

import xyz.niyin.graduate.entity.Productlist;
import xyz.niyin.graduate.entity.Store;
import xyz.niyin.graduate.mapper.ProductimgsMapper;
import xyz.niyin.graduate.mapper.ProductlistMapper;
import xyz.niyin.graduate.mapper.StoreMapper;
import xyz.niyin.graduate.utils.BosUtil;
import xyz.niyin.graduate.utils.Result;



@RestController
@RequestMapping("/search")
public class SearchController {
	@Autowired
	ProductlistMapper productlistMapper;
	@Autowired
	ProductimgsMapper productimgsMapper;
	@Autowired
	StoreMapper storeMapper;
@RequestMapping("/getsearchpro")
public JSONObject getpro(String protype,String distance,String inp,double latitude,double longitude) {
	JSONObject data=new JSONObject();
	System.out.println(latitude);
	System.out.println(longitude);
	//System.out.println(protype);
	//System.out.println(distance);
	//System.out.println(inp);
	JSONObject obj=new JSONObject();
	BosUtil bosUtil=new BosUtil();
	JSONArray dataArray=new JSONArray();
	if(protype.equals("无")&&distance.equals("无")) {
		if(inp=="") {
			obj.put("msg",0);
		}else {
		//直接执行查询
		String productName=inp;
		productlistMapper.selectProductsearch(productName);
		List<Productlist> proList=productlistMapper.selectProductsearch(productName);
		for(int j=0;j<proList.size();j++) {
			JSONObject proinfo=new JSONObject();
			String productId = proList.get(j).getProductId();
			String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
			String logo=bosUtil.getimgurl(imgname);
			proinfo.put("pro",proList.get(j));
			proinfo.put("logo",logo);
			dataArray.add(proinfo);
		}
		obj.put("dataArray",dataArray);
		if(proList.size()==0) {
			obj.put("msg",2);
		}else {
			obj.put("msg",1);
		}
		}
	}
	if(!protype.equals("无")&&distance.equals("无")) {
		System.out.println("ssaaasss");
		String productName=inp;
		productlistMapper.selectProductsearch(productName);
		String productType=protype;
		List<Productlist> proList=productlistMapper.selectProductsearchtype(productType, productName);
		System.out.println(productlistMapper.selectProductsearch(productName));
		//执行查询
		//查询商品
		if(proList.size()==0) {
			obj.put("msg",2);
		}else {
			for(int j=0;j<proList.size();j++) {
				JSONObject proinfo=new JSONObject();
				String productId = proList.get(j).getProductId();
				String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
				System.out.println(bosUtil.getimgurl(imgname));
				String logo=bosUtil.getimgurl(imgname);
				proinfo.put("pro",proList.get(j));
				proinfo.put("logo",logo);
				dataArray.add(proinfo);
			}
			obj.put("dataArray",dataArray);
			obj.put("msg",1);
		}

	}
	if(protype.equals("无")&&!distance.equals("无")) {
		//System.out.println("sssss");

		//执行查询
		    //查询商家id
				//JSONObject data = new JSONObject();
				//double latitude=22.995083;
				//double longitude=113.783088;
		        //先计算查询点的经纬度范围
		        System.out.println(distance);
		        String a = distance.substring(1);
		        System.out.println(a);
		        String a1=a.substring(0,a.indexOf("k"));
		        double d=Double.parseDouble(a1);
		        System.out.println(d);
		        double r = 6371;//地球半径千米
		        double dis = d;//0.5千米距离
		        double dlng =  2*Math.asin(Math.sin(dis/(2*r))/Math.cos(latitude*Math.PI/180));
		        dlng = dlng*180/Math.PI;//角度转为弧度
		        double dlat = dis/r;
		        dlat = dlat*180/Math.PI;
		        double minlat =latitude-dlat;
		        double maxlat = latitude+dlat;
		        double minlng = longitude -dlng;
		        double maxlng = longitude + dlng;
		        String aminlat=String.format("%.6f", minlat);
		        String amaxlat=String.format("%.6f", maxlat);
		        String aminlng=String.format("%.6f", minlng);
		        String amaxlng=String.format("%.6f", maxlng);
				System.out.println("minlat:"+aminlat);
				System.out.println("maxlat"+amaxlat);
				System.out.println("minlng"+aminlng);
				System.out.println("maxlng"+amaxlng);
				Result result=new Result();
				List<Store> arr=storeMapper.selectnearby(minlat, maxlat, minlng, maxlng);
				System.out.println(arr);
				if(arr.size()==0) {
					System.out.println("附近没有你需要的商品哦");
					obj.put("msg",2);
				}
				for (int i = 0; i < arr.size(); i++) {
					System.out.println("store-----");
					System.out.println(arr.get(i).getId());
					int storeid=arr.get(i).getId();
					//查询数据库
					String productName=inp;
					System.out.println(productlistMapper.selectProductsearchdis(storeid, productName));
					List<Productlist> proList=productlistMapper.selectProductsearchdis(storeid, productName);
					if(proList.size()==0) {
						System.out.println("附近没有你需要的商品哦");
						//obj.put("msg",2);
					}else {
						obj.put("msg",1);
						for(int j=0;j<proList.size();j++) {
							JSONObject proinfo=new JSONObject();
							String productId = proList.get(j).getProductId();
							String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
							System.out.println(bosUtil.getimgurl(imgname));
							String logo=bosUtil.getimgurl(imgname);
							proinfo.put("pro",proList.get(j));
							proinfo.put("logo",logo);
							dataArray.add(proinfo);
						}
					}
				}
				System.out.println(dataArray+"---------------");
				if(dataArray.size()==0) {
					obj.put("msg",2);
				}
				obj.put("dataArray",dataArray);
	}
	if(!protype.equals("无")&&!distance.equals("无")) {
		 System.out.println("yyyyy");
		 System.out.println(distance);
	        String a = distance.substring(1);
	        System.out.println(a);
	        String a1=a.substring(0,a.indexOf("k"));
	        double d=Double.parseDouble(a1);
	        System.out.println(d);
	        double r = 6371;//地球半径千米
	        double dis = d;//0.5千米距离
	        double dlng =  2*Math.asin(Math.sin(dis/(2*r))/Math.cos(latitude*Math.PI/180));
	        dlng = dlng*180/Math.PI;//角度转为弧度
	        double dlat = dis/r;
	        dlat = dlat*180/Math.PI;
	        double minlat =latitude-dlat;
	        double maxlat = latitude+dlat;
	        double minlng = longitude -dlng;
	        double maxlng = longitude + dlng;
	        String aminlat=String.format("%.6f", minlat);
	        String amaxlat=String.format("%.6f", maxlat);
	        String aminlng=String.format("%.6f", minlng);
	        String amaxlng=String.format("%.6f", maxlng);
			System.out.println("minlat:"+aminlat);
			System.out.println("maxlat"+amaxlat);
			System.out.println("minlng"+aminlng);
			System.out.println("maxlng"+amaxlng);
			Result result=new Result();
			List<Store> arr=storeMapper.selectnearby(minlat, maxlat, minlng, maxlng);
			System.out.println(arr);
			if(arr.size()==0) {
				System.out.println("附近没有你需要的商品哦");
				obj.put("msg",2);
			}

			for (int i = 0; i < arr.size(); i++) {
				System.out.println("store-----");
				System.out.println(arr.get(i).getId());
				String storeid=arr.get(i).getId()+"";
				//查询数据库
				String productName=inp;
				//System.out.println(productlistMapper.selectProductsearchdis(storeid, productName));
				String productType=protype;
				System.out.println(productType+"====="+storeid+"-----"+productName);
				List<Productlist> proList=productlistMapper.selectProductsearchdistype(productType,storeid, productName);
				if(proList.size()==0) {
					System.out.println("附近没有你需要的商品哦");
					//obj.put("msg",2);
				}else {
					obj.put("msg",1);
					for(int j=0;j<proList.size();j++) {
						JSONObject proinfo=new JSONObject();
						String productId = proList.get(j).getProductId();
						String imgname=productimgsMapper.selectProductimgone(productId).getImgname();
						System.out.println(bosUtil.getimgurl(imgname));
						String logo=bosUtil.getimgurl(imgname);
						proinfo.put("pro",proList.get(j));
						proinfo.put("logo",logo);
						dataArray.add(proinfo);
					}
				}
			}
			if(dataArray.size()==0) {
				obj.put("msg",2);
			}
			obj.put("dataArray",dataArray);

	}
	data.put("code",0);
	data.put("data",obj);
	data.put("message","");
	return data;
}
}
