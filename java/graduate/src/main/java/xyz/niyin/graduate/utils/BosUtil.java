package xyz.niyin.graduate.utils;

import java.net.URL;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.bos.BosClient;
import com.baidubce.services.bos.BosClientConfiguration;

import lombok.Data;

@Data
@ConfigurationProperties("bos.config")
public class BosUtil {
	private String ACCESS_KEY_ID="xxx";
	private String SECRET_ACCESS_KEY="xxx";
	private String ENDPOINT="bj.bcebos.com";
public  String getimgurl(String imgname){
	  // 初始化一个BosClient
    BosClientConfiguration config = new BosClientConfiguration();
    config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
    config.setEndpoint(ENDPOINT);
    BosClient client = new BosClient(config);
    URL url = client.generatePresignedUrl("photo-reports",imgname,3600);
    String  urlString=url.toString();
    String reString=urlString.substring(4);
	return "https"+reString;	
}
public  String getimgurls(String photo_reports,String imgname){
    // 初始化一个BosClient
	  // 初始化一个BosClient
    BosClientConfiguration config = new BosClientConfiguration();
    config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID, SECRET_ACCESS_KEY));
    config.setEndpoint(ENDPOINT);
    
    BosClient client = new BosClient(config);
    
    URL url = client.generatePresignedUrl(photo_reports,imgname,3600);
    String  urlString=url.toString();
    String reString=urlString.substring(4);
    //System.out.println(reString);
    //System.out.println("https"+reString);
	return "https"+reString;
	
}

}
