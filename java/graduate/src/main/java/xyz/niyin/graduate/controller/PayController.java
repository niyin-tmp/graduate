package xyz.niyin.graduate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.baidubce.services.cdn.model.JsonObject;

import xyz.niyin.graduate.mapper.OrderMapper;
import xyz.niyin.graduate.mapper.UserMapper;
import xyz.niyin.graduate.service.PayService;
import xyz.niyin.graduate.utils.JWTUtils;

@RequestMapping("/pay")
@RestController
public class PayController {
	@Autowired
	PayService payService;
	@Autowired
	OrderMapper orderMapper;
	@Autowired
	UserMapper userMapper;
@RequestMapping("/payzf")
public JSONObject pay(String name,String phone,String address,String token,String cartlist) {
	System.out.println("---------");
	System.out.println("姓名"+name+"手机"+phone+"地址："+address);
	JSONObject jsobj1=new JSONObject();
	//System.out.println(token);
	DecodedJWT verify= JWTUtils.getTokenInfo(token);
	String openid=verify.getClaim("openid").asString();
	System.out.println(userMapper.selectUser(openid));
	String id=userMapper.selectUser(openid).getId()+"";
	System.out.println(id);
	//产生3位随机数
	String string=(int)((Math.random()*9+1)*100)+"";
	System.out.println(string);
	//获取时间chuo
	Long startTs = System.currentTimeMillis();
	System.out.println(startTs);
	//获取商品id
	
	//System.out.println(cartlist);
    JSONArray  jsonarray=JSONArray.parseArray(cartlist);
    double allpirce=0.00;
    String orderNo="";
    for(int i=0;i<jsonarray.size();i++) {
    	System.out.println("--------------------");
    	//System.out.println(jsonarray.getJSONObject(i));
    	//System.out.println(jsonarray.getJSONObject(i).get("checked"));
    	if((boolean) jsonarray.getJSONObject(i).get("checked")) {
    		
    		System.out.println(jsonarray.getJSONObject(i).get("num"));
    		//商品数量
    		int num=(int) jsonarray.getJSONObject(i).get("num");
    		System.out.println(jsonarray.getJSONObject(i).getJSONObject("Productinfo").get("productPrice"));
    		double price=(double)Double.parseDouble((String) jsonarray.getJSONObject(i).getJSONObject("Productinfo").get("productPrice"));
    		//商品总价
    		double numprice=num*price;
    		//支付总价
    		allpirce=allpirce+num*price;
    		//storeid
    		String storeid=(String) jsonarray.getJSONObject(i).getJSONObject("Productinfo").get("storeid");
    		String productId=(String) jsonarray.getJSONObject(i).getJSONObject("Productinfo").get("productId");
    		String proname=(String) jsonarray.getJSONObject(i).getJSONObject("Productinfo").get("productName");
        	System.out.println(jsonarray.getJSONObject(i).getJSONObject("Productinfo").get("productId"));
        	//商品id
        	String proid=(int) jsonarray.getJSONObject(i).getJSONObject("Productinfo").get("id")+"";
        	System.out.println(proid);
        	//商品单号
        	String ordernum=id+startTs+proid+"";
        	System.out.println(ordernum);
        	//支付单号
        	 orderNo=id+startTs+"";
        	 System.out.println(orderNo);
        	//订单状态
        	 String status="0";
        	//写进数据库
        	 orderMapper.insertOrder(openid,storeid, ordernum, orderNo, status, productId,proname, num, price, numprice,name,phone,address);
    	}
    }
    System.out.println(allpirce);
	String amount=allpirce+"";
	
	String resultString =payService.pay(amount, orderNo);
	JSONObject jsonObject = JSONObject.parseObject(resultString);
	System.out.println(jsonObject.getJSONObject("data").get("payUrl"));
	String reString=(String) jsonObject.getJSONObject("data").get("payUrl");
	
	
	jsobj1.put("code", 0);
	//jsobj1.put("data", reString);
	jsobj1.put("data", reString);
	jsobj1.put("message", "");
	return jsobj1;
}
@RequestMapping("/payinfo")
public JSONObject Payinfo(String name,String phone,String address,int num,String token,String productinfo) {
	JSONObject jsobj1=new JSONObject();
	System.out.println(name);
	System.out.println(phone);
	System.out.println(address);
	System.out.println(token);
	System.out.println(productinfo);
	JSONObject jsonObject = JSONObject.parseObject(productinfo);
	System.out.println(jsonObject.get("productId"));

	//openid
	DecodedJWT verify=JWTUtils.getTokenInfo(token);
	String openid=verify.getClaim("openid").asString();
	//店铺id
    String storeid=(String) jsonObject.get("storeid");
	String id=userMapper.selectUser(openid).getId()+"";
	//获取时间戳
	Long startTs = System.currentTimeMillis();
	String proid=jsonObject.get("id")+"";
	//商品单号
	String ordernum=id+startTs+proid+"";
	//支付单号
	String orderNo=id+startTs+"";
	//订单状态
	String status="0";
	//商品id
	String productId=(String) jsonObject.get("productId");
	//价格
	double price=(double)Double.parseDouble((String) jsonObject.get("productPrice"));
	double numprice=num*price;
	String proname=(String) jsonObject.get("productName");
	//写进数据库
	 orderMapper.insertOrder(openid,storeid, ordernum, orderNo, status, productId,proname, num, price, numprice,name,phone,address);
	 String amount=numprice+"";
	String resultString =payService.pay(amount, orderNo);
	JSONObject jsonO = JSONObject.parseObject(resultString);
	String reString=(String) jsonO.getJSONObject("data").get("payUrl");
	jsobj1.put("code", 0);
	//jsobj1.put("data", reString);
	jsobj1.put("data", reString);
	jsobj1.put("message", "");
	return jsobj1;
}
@RequestMapping("/tonopay")
public JSONObject nopay(String token,String orderNo, String numprice) {
	JSONObject jsobj1=new JSONObject();	
	String amount=numprice;
	String resultString =payService.pay(amount, orderNo);
	JSONObject jsonObject = JSONObject.parseObject(resultString);
	System.out.println(jsonObject.getJSONObject("data").get("payUrl"));
	String reString=(String) jsonObject.getJSONObject("data").get("payUrl");
	jsobj1.put("code", 0);
	//jsobj1.put("data", reString);
	jsobj1.put("data", reString);
	jsobj1.put("message", "");
	return jsobj1;
}
}
