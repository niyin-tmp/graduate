package xyz.niyin.graduate.entity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author lastwhisper
 * @desc
 * @email gaojun56@163.com
 */
@Data
public class User {
	private int id;

    private String openId;

    private String nickName;
    
    private String avatarUrl;
    
    private String phone;
    
    private String email;
    
    private String password;
    
    private String remark;
}

