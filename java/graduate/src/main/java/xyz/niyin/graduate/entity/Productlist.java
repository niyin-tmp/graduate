package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class Productlist {
	private int Id;
	private String productId;
	private String storeid;
	private String productName;
	private String productNum;
	private String productPrice;
	private String productType;
	private String productExplain;
	private int pageviews;
}
