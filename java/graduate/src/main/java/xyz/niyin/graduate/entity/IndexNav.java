package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class IndexNav {
	private int id;
	private String image_name;
	private String type_name;
	private String  navigator_url;
}
