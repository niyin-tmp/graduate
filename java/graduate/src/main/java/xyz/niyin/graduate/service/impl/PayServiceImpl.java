package xyz.niyin.graduate.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import xyz.niyin.graduate.service.PayService;
import xyz.niyin.graduate.utils.MD5Utils;


@Service
public class PayServiceImpl implements PayService {
	@Override
	public String pay(String amount,String orderNo) {
		String merchantNum = "xxx";// 商户号
		//String orderNo = "20190629023134U93661109";// 商户订单号
		//String amount = "0.20";// 支付金额
		String notifyUrl = "https://www.niyin.xyz:8889/pay/sucpays";// 填写您的接收支付成功的异步通知地址
		String returnUrl = "";// 同步通知地址
		String payType = "wechat";// 请求支付类型 
		String attch = ""; // 附加参数
		String secretKey = "xxx";//商户密钥
		String sign = merchantNum + orderNo + amount + notifyUrl + secretKey;
		sign = MD5Utils.md5(sign);// md5签名
	    System.out.println(sign);
		String url = "http://api-af49xmrptzwg.zhifu.fm.it88168.com/api/startOrder"; //"http://zfapi.nnt.ltd/api/startOrder";// 发起订单地址
		Map<String, String> paramMap = new HashMap<>();// post请求的参数
		paramMap.put("merchantNum", merchantNum);
		paramMap.put("orderNo", orderNo);
		paramMap.put("amount", amount);
		paramMap.put("notifyUrl", notifyUrl);
		paramMap.put("returnUrl", returnUrl);
		paramMap.put("payType", payType);
		paramMap.put("attch", attch);
		paramMap.put("sign", sign);
		paramMap.put("subject", "测试subject");
		paramMap.put("body", "测试商品body");
		System.out.println(JSONObject.toJSON(paramMap));
        String result="null";
        JSONObject ret = new JSONObject();
        try {
            String paramStr = toParams(paramMap);
            System.out.println(paramStr);
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
            HttpPost httpost = new HttpPost(url + "?" + paramStr); // 设置响应头信息
            String userAgent = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36";
            httpost.setHeader("User-Agent",userAgent); //防止被防火墙拦截 Apache httpclient
            
            RequestConfig requestConfig = RequestConfig.custom()  
                .setConnectTimeout(5000).setConnectionRequestTimeout(1000)  
                .setSocketTimeout(5000).build();  
            httpost.setConfig(requestConfig); 
            
            HttpResponse retResp = httpclient.execute(httpost);
            result = EntityUtils.toString(retResp.getEntity(), "UTF-8");
            System.out.println(result);
        }
        catch (ClientProtocolException e1) {
            e1.printStackTrace();
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }catch (ParseException e1) {
            e1.printStackTrace();
        }
    	return result;	
	}
    public String toParams(Map<String, String> params) throws UnsupportedEncodingException {
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);
        String prestr = "";
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);
            value = URLEncoder.encode(value, "UTF-8");
            if (i == keys.size() - 1) {// 拼接时，不包括最后一个&字符
                prestr = prestr + key + "=" + value;
            }
            else {
                prestr = prestr + key + "=" + value + "&";
            }
        }
        return prestr;
    }
}
