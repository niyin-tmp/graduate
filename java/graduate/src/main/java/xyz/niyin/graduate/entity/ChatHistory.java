package xyz.niyin.graduate.entity;

import lombok.Data;

@Data
public class ChatHistory {
private int id;
private String useropenid;
private String storeopenid;
private String content;
private String status;
private String userstatus;
private String contype;

}
